# A .Net Framework #

This repository contains a simple .NET framework for building services. Some of the features of this framework include:

* **Framework** includes:
    * Logging integrated with exception handling, logging correlation, queued logging.
    * Worker threads.
    * Extended exceptions
* **WindowsServiceHost** hosts custom service implementations. 
* **WcfService** A custom service implementation that creates and manages pluggable WCF services.
* A **Service Publisher** (bus) containing
    * **DelivererModel** enables WcfServices publish messages to subscribers.
    * **SubscribersRepository** maps publishers to subscribers
    * **SubscribersServiceLibrary** provides service-based control to **SubscribersLibrary.

## Example

A complete example is contained in Infrastructure/DeliveryServices


## Service Publisher Setup

### Overview

Foundation/DeliveryModel contains a lean WCF service bus. A complete usage example is contained in Infrastructure/DeliveryServices. This document describes how to setup the service bus using Infrastructure/DeliveryServices as an example.

### Dependencies

The Service Bus requires .NET V4.0 or higher. 

### Service Executable
1. Gather the following information:
    1. *ServiceExecutable** The name of the service executable. 
Example: ItemPublishService.exe.
    1. *ServiceExecutablePath* The local directory where the ServiceExecutable resides.
Example: c:\repo\DeliveryServices\bin\release
    1. *WindowsDomainUser*. The name of the domain user under which context ServiceExecutable will run.
Example: mainoffice\ServiceBusUser.
    1. *ServiceExecutableConfiguration* The name of the ServiceExecutable's configuration file, which must also reside in ServiceExecutablePath.
Example: ItemPublishService.exe.config
    1. *DatabaseName*: The name of the database in SQL Server.
Example: ServiceBus
    1. *HostDnsName*: The DNS name of the server hosting SQL Server.
Example: server.com
    1. *ServiceDisplayName*: The name displayed in the Windows Service Controller for the ServiceExecutable.
Example: Item Publish Service
1. Set up a *WindowsDomainUser* with password that never expires and doesn't need changing. 
1. Run ServiceExecutable “as administrator” once to add event log sources.
1. Add WindowsDomainUser to the database:
    1. Add WindowsDomainUser to SQL Server instance:
        1. Connect to SQL Server Instance.
        1. Navigate to the “Security folder”, 
        1. Right-click “Logins”, 
        1. Choose “New Login”. 
        1. On the “General” page, click the Search button to browse for a local or domain login or group. The Search button opens the Select User or Group dialog box, allowing you to query for local or domain users and groups. Select theWindowsDomainUser .  
        1. Ensure Windows authentication option is selected.    
        1. Click OK to close the “Login – New” dialog box.
    1. Add SQL Server User to database and associate it with WindowsDomainUser :
        1, In the Object Explorer, expand the database the user needs to access. 
        1. Expand the Security folder under the database.  
        1. Right-click the Users folder and click New User.  
        1, Type a name for the user in the User name field. Click the ellipsis button next to the Login name field to browse for the WindowsDomainUser added to the SQL Server instance.
        1. Assign ‘default schema’ to ‘dbo’.
        1. Ensure ‘db_owner’ in ‘database role membership’ list is checked.
1. In Windows Server 2003, run Executable not as administrator and select "do not do this check again" in security dialog that appears.
1. Edit ServiceExecutableConfiguration by opening file in Notepad.exe and making the following edits:
    1. Set connection string to database:
    <connectionStrings>
        <add name="SubscribersModelContainer" connectionString="metadata=res://*/SubscribersModel.csdl|res://*/SubscribersModel.ssdl|res://*/SubscribersModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;Data Source=HostDnsName;Initial Catalog=DatabaseName;Integrated Security=True;MultipleActiveResultSets=True&quot;" providerName="System.Data.EntityClient" />
    </connectionStrings>
    1. Set ServiceExecutable so it can be run by the Windows Service Controller:
    <add key="Service.isService" value="true" />
    1. for ItemPublishService.exe.config, edit the following path to match the location of the ItemPublishService.exe.config file being edited:
    <add key="AuditLog.AbsoluteDirectory" value="ServiceExecutablePath"/>
1. Install the ServiceExecutable into the Windows Service Controller:
    1. Open command prompt as administrator
    1. Set the following to the name you want the service to be displayed as in the Windows Service Controller:
set SERVICE_NAME=ServiceDisplayName
    1. Run the Windows Service Controller installer and supply the ServiceExecutable exe path as the parameter:
C:\Windows\Microsoft.NET\Framework\v4.0.30319\installutil.exe c:\ServiceExecutablePath\ServiceExecutable.exe
1. Configure the Windows Service
    1. Open the Windows Service Controller applet
    1. right click and select properties over the service named ServiceDisplayName
    1. change service's logonas to the  WindowsDomainUser
1. Give the Windows Domain User “full control” over the  ServiceExecutablePath:
    1. Open Windows Explorer
    1. navigate to  ServiceExecutablePath.
    1. Right click, select properties
    1. Select security tab
    1. Click edit, then add, 
    1. add Windows Domain User and check “ful control”.

### Subscriber Database
The SQL Server database is set up from a DDL script, which is then configured with the following information mapping subscribers to publish services:

* **Contracts**: Identifies the .NET interface contracts
* **Operations**: Identifies the methods for each interface that subscribers can subscribe to.                                            
* **Services**:Identifies the Publisher services.                                    
* **ServiceContractsAssocs**: Identifies which interface contract(s) each service provides.
* **Subscribers**: Maps which operation in which contract on which service each subscriber is subscribed to.

### Addendum: Example Service Map Database setup

**Contracts**:    
com.example.delivery.IItemsChanged                                                         

**Operations**:    
allFinishedGoodItems        com.example.delivery.IItemsChanged                                                         

**Services**:     
com.example.delivery.ItemsChangedPublishService                                                                                                   

**ServiceContractsAssocs**:    
com.example.delivery.ItemsChangedPublishService        com.example.delivery.IItemsChanged  

**Subscribers**:
(Address
OperationName
OperationContract
ServiceTypeFullname)

local://ItemSubscribers.dll/com.example.delivery.ItemsLocalSubscriber  
allFinishedGoodItems    
com.example.delivery.IItemsChanged                                                         
com.example.delivery.ItemsChangedPublishService    
False

http://localhost:9998/ItemsSubscriberService    
allFinishedGoodItems    
com.example.delivery.IItemsChanged                                                         
com.example.delivery.ItemsChangedPublishService    
False