/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Reflection;
using System.Runtime.Remoting;



namespace net.compasspoint
{
	public class Service : System.ServiceProcess.ServiceBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
//		private System.Diagnostics.EventLog eventLogger;
		private IService service;

		public Service()
		{
			// This call is required by the Windows.Forms Component Designer.
/*
            this.eventLogger = new System.Diagnostics.EventLog();
			((System.ComponentModel.ISupportInitialize)(this.eventLogger)).BeginInit();

			string sourceEventString = AppResources.getConfigurationString("Service.eventsourcestring");
			if(!System.Diagnostics.EventLog.SourceExists(sourceEventString))
				System.Diagnostics.EventLog.CreateEventSource(sourceEventString, "Application");

			// 
			// eventLogger
			// 
			this.eventLogger.EnableRaisingEvents = true;
			this.eventLogger.Log = "Application";
			this.eventLogger.Source = sourceEventString;
			((System.ComponentModel.ISupportInitialize)(this.eventLogger)).EndInit();
*/
			InitializeComponent();

			// TODO: Add any initialization after the InitComponent call
		}

		// The main entry point for the Process
        static void Main(string[] args)
		{
			bool isService = Convert.ToBoolean(AppResources.getConfigurationString("Service.isService"));

			if(isService) 
			{
				System.ServiceProcess.ServiceBase[] ServicesToRun;
	
				// More than one user Service may run within the same Process. To add
				// another service to this Process, change the following line to
				// create a second service object. For example,
				//
				//   ServicesToRun = New System.ServiceProcess.ServiceBase[] {new Service1(), new MySecondUserService()};
				//
                ServicesToRun = new System.ServiceProcess.ServiceBase[] { new net.compasspoint.Service() };

				System.ServiceProcess.ServiceBase.Run(ServicesToRun);
			}
			else 
			{

				(new Service()).OnStart(args);
			}
		}

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// Service
			// 
			this.CanShutdown = true;
			this.ServiceName = AppResources.getConfigurationString("Service.servicename");

		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		/// <summary>
		/// Set things in motion so your service can do its work.
		/// </summary>
		protected override void OnStart(string[] args)
		{
			//Start the service
			bool isService = Convert.ToBoolean(AppResources.getConfigurationString("Service.isService"));
			string serviceClassName = null;

            string assemblyName = null;

            if (isService)
            {
                assemblyName = AppResources.getConfigurationString("ServiceAssemblyName");
                serviceClassName = AppResources.getConfigurationString("ServiceClassNameService");
            }
            else
            {
                assemblyName = AppResources.getConfigurationString("ServiceAssemblyNameConsole");
                serviceClassName = AppResources.getConfigurationString("ServiceClassNameConsole");
            }

            ObjectHandle obj = Activator.CreateInstance(assemblyName, serviceClassName);
			service = (IService) obj.Unwrap();

            Logger.submitToLog(new DiagnosticsLogMessage(System.Configuration.ConfigurationManager.AppSettings["Service.strStarting"], new ComponentIdentity(this.GetType().FullName), "OnStart()")); 
            
            try
			{
                string instanceId = "";

                if (service is IServiceEx)
                {
                    instanceId = ((IServiceEx)service).Startup(args);
                }
                else
                {
                    instanceId = service.Startup();
                }


                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

                string assemblyStrings = "\n";

                foreach (Assembly assembly in assemblies)
                {
                    if (!assembly.GlobalAssemblyCache)
                    {
                        assemblyStrings += assembly.GetName().FullName + ";\n";
                    }
                }
                EventLog.WriteEntry(
                    AppResources.getConfigurationString("Service.eventsourcestring"),
                    string.Format(System.Configuration.ConfigurationManager.AppSettings["Service.started"], "\n", assemblyStrings, instanceId), 
                    EventLogEntryType.Information);
                //Logger.submitToLog(new DiagnosticsLogMessage(string.Format(System.Configuration.ConfigurationManager.AppSettings["Service.started"], "\n", assemblyStrings, instanceId), new ComponentIdentity(this.GetType().FullName), "OnStart()"));
            }
			catch (AppException aex)
			{
//				Logger.submitToLog(aex);

                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

                string assemblyStrings = "\n";

                foreach (Assembly assembly in assemblies)
                {
                    if (!assembly.GlobalAssemblyCache)
                    {
                        assemblyStrings += assembly.GetName().FullName + ";\n";
                    }
                }

                EventLog.WriteEntry(AppResources.getConfigurationString("Service.eventsourcestring"),
                    string.Format(System.Configuration.ConfigurationManager.AppSettings["Service.error"], "\n", assemblyStrings, aex.getMessage(), aex.ToString()), 
                    EventLogEntryType.Error, 
                    1);

                //Logger.submitToLog(new ErrorLogMessage((string.Format(System.Configuration.ConfigurationManager.AppSettings["Service.error"], "\n", assemblyStrings, aex.getMessage(), aex.ToString())), new ComponentIdentity(this.GetType().FullName), "OnStart()"));
            }
			catch (Exception ex)
			{	

                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

                string assemblyStrings = "\n";

                foreach (Assembly assembly in assemblies)
                {
                    if (!assembly.GlobalAssemblyCache)
                    {
                        assemblyStrings += assembly.GetName().FullName + ";\n";
                    }
                }

                EventLog.WriteEntry(AppResources.getConfigurationString("Service.eventsourcestring"),
                    string.Format(System.Configuration.ConfigurationManager.AppSettings["Service.error"], "\n", assemblyStrings, ex.Message, ex.ToString()), 
                    EventLogEntryType.Error, 
                    1);
				//Logger.submitToLog(new ErrorLogMessage(string.Format(System.Configuration.ConfigurationManager.AppSettings["Service.error"], "\n", assemblyStrings, ex.Message, ex.ToString()), new ComponentIdentity(this.GetType().FullName), "OnStart"));
                if (ex.InnerException != null)
                {
                    EventLog.WriteEntry(AppResources.getConfigurationString("Service.eventsourcestring"),
                        string.Format(System.Configuration.ConfigurationManager.AppSettings["Service.error"], "\n", assemblyStrings, ex.Message, ex.ToString()), 
                        EventLogEntryType.Error, 
                        1);
                    //Logger.submitToLog(new ExceptionLogMessage(ex.InnerException, new ComponentIdentity(this.GetType().FullName), "OnStart"));
                }

            }
			finally
			{
			}
		}
 
		/// <summary>
		/// Stop this service.
		/// </summary>
		protected override void OnStop()
		{

            Logger.submitToLog(new DiagnosticsLogMessage(System.Configuration.ConfigurationManager.AppSettings["Service.strStopping"], new ComponentIdentity(this.GetType().FullName), "OnStop"));

			try
			{
				service.Shutdown();
			}
			catch (AppException aex)
			{
				Logger.submitToLog(new ErrorLogMessage("BUG: service::shutdown() should never exception! See exception that follows.", new ComponentIdentity(this.GetType().FullName), "OnStop"));
				Logger.submitToLog(aex);
			}
			catch (Exception ex)
			{	
				Logger.submitToLog(new ExceptionLogMessage(ex, new ComponentIdentity(this.GetType().FullName), "OnStop"));
			}
			Logger.shutdown();
		}

		protected override void OnShutdown()
		{
			this.OnStop();
		}
	}
}
