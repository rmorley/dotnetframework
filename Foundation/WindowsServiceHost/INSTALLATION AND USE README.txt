TO Install as service:

INSTALLATION:
1. Run the application in console mode (with Service.isService set to "false" in the app.config) once as administrator to create the EventLog source.
2. At the command prompt, type "set SERVICE_NAME=ServicesName". If this is not set the service's name defaults to "CompassPoint Windows Service".
3. Set "Service.isService" to "true" in the app.config
4. EXECUTE (Path to your installutil.exe .NET Framework utility may very):
  C:\Windows\Microsoft.NET\Framework\v4.0.30319\installutil.exe [WindowsServiceHost].exe
  
  Note that in Windows Vista or newer, the above must be executed as 'Administrator'.  

  Note: may need to run [WindowsServiceHost].exe with app.config Service.isService set to "false" and not as administrator in order to register program as valid (Windows may have detected it came from
  a remote source).

UNINSTALL:
1. At the command prompt, type "set SERVICE_NAME=ServicesName". If this is not set the following command attempts to remove a service named "CompassPoint Windows Service".
2. EXECUTE: C:\Windows\Microsoft.NET\Framework\v4.0.30319\installutil.exe /u [WindowsServiceHost].exe

USE: 
Go to ControlPanel | Computer Management | Services and find ServiceName. From here you can configure its properties
to auto-Start and Start and Stop the service.


SERVICE USER ACCOUNT ('LogOn' tab):
For use of NETWORK SERVICE, see http://msdn.microsoft.com/en-us/library/ff647402.aspx
For differences between LOCAL SYSTEM, LOCAL SERVICE, and NETWORK SERVICE, see http://stackoverflow.com/questions/510170/the-difference-between-the-local-system-account-and-the-network-service-acco
Additional notes on self-hosted Windows services at: http://msdn.microsoft.com/en-us/library/bb332338.aspx
