﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace net.compasspoint.delivery
{

    [ServiceContract(SessionMode = SessionMode.Required)]//NOTE: requires the transport to support reliable delivery, either TCP, ReliableSessionBinding element, or MSMQ. See http://msdn.microsoft.com/en-us/library/ms733040.aspx
    public interface ISubscribe
    {
        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void subscribe(string subscriberAddress, string serviceTypefullname, string contractTypefullname, string operationName);

        [OperationContract]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void unsubscribe(string subscriberAddress, string serviceTypefullname, string contractTypefullname, string operationName);

    }
}
