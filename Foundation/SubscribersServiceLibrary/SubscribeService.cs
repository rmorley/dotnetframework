﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace net.compasspoint.delivery
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class SubscribeService : ISubscribe
    {
        #region ISubscribe

        //[OperationBehavior(TransactionScopeRequired = true)] if a transaction scope is required
        public void subscribe(string subscriberAddress, string serviceTypefullname, string contractTypefullname, string operationName)
        {
            //NOTE: Opening a SqlConnection within a TransactionScope automatically enlists it
            // in the TransactionScope  http://msdn.microsoft.com/en-us/library/system.transactions.transactionscope.aspx
            SubscribersRepository.getInstance().subscribe(subscriberAddress, serviceTypefullname, contractTypefullname, operationName);
        }

        //[OperationBehavior(TransactionScopeRequired = true)] if a transaction scope is required
        public void unsubscribe(string subscriberAddress, string serviceTypefullname, string contractTypefullname, string operationName)
        {
            SubscribersRepository.getInstance().unsubscribe(subscriberAddress, serviceTypefullname, contractTypefullname, operationName);
        }

         #endregion

    }
}
