﻿/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace net.compasspoint.delivery
{
    public class SubscribersRepository
    {
        //singleton for purposes of supporting derrivative implementations, NOT TO MAINTAIN INSTANCE-SPECIFIC STATE.
        private static SubscribersRepository instance = null;

        public static SubscribersRepository getInstance()
        {
            if (instance == null)
            {
                instance = new SubscribersRepository();
            }
            return instance;
        }

        protected SubscribersRepository()
        {
        }


        public virtual List<string> getAddressesOfSubscribersOfOperation(string serviceTypefullname, string contractTypefullname, string operationName, out bool firstWantsToReturn)
        {
            firstWantsToReturn = false;
            //if more than wants to return, throw new MoreThanOneSubscriberWantsToReturn()
            using (SubscribersModelContainer context = new SubscribersModelContainer())
            {

                var subscriberAddresses = from s in context.Subscribers
                              where s.ServiceTypefullname.Equals(serviceTypefullname) && s.OperationContractTypefullname.Equals(contractTypefullname) && s.OperationName.Equals(operationName)
                              select s.Address.Trim();

                return new List<string>(subscriberAddresses);
            }
        }

        public virtual void subscribe(string subscriberAddress, string serviceTypefullname, string contractTypefullname, string operationName)
        {
            //FIXME: complete

        }

        public virtual void unsubscribe(string subscriberAddress, string serviceTypefullname, string contractTypefullname, string operationName)
        {
            //FIXME: complete

        }

        public virtual void registerContract(string contractTypefullname, string[] operationNames)
        {
            //FIXME: complete

        }

        public virtual void registerServiceForContract(string serviceTypefullname, string contractTypefullname, bool supportsMoreThanOneSubscriber)
        {
            //FIXME: complete

        }

    }
}
