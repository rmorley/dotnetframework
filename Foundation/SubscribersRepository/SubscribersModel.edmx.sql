
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 11/13/2012 12:13:03
-- Generated from EDMX file: SubscribersModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Subscribers];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ServiceServiceContractAssoc]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServiceContractAssocs] DROP CONSTRAINT [FK_ServiceServiceContractAssoc];
GO
IF OBJECT_ID(N'[dbo].[FK_ContractServiceContractAssoc]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ServiceContractAssocs] DROP CONSTRAINT [FK_ContractServiceContractAssoc];
GO
IF OBJECT_ID(N'[dbo].[FK_ContractOperation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Operations] DROP CONSTRAINT [FK_ContractOperation];
GO
IF OBJECT_ID(N'[dbo].[FK_OperationSubscriber]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Subscribers] DROP CONSTRAINT [FK_OperationSubscriber];
GO
IF OBJECT_ID(N'[dbo].[FK_ServiceSubscriber]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Subscribers] DROP CONSTRAINT [FK_ServiceSubscriber];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Contracts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Contracts];
GO
IF OBJECT_ID(N'[dbo].[Operations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Operations];
GO
IF OBJECT_ID(N'[dbo].[Subscribers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Subscribers];
GO
IF OBJECT_ID(N'[dbo].[Services]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Services];
GO
IF OBJECT_ID(N'[dbo].[ServiceContractAssocs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ServiceContractAssocs];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Contracts'
CREATE TABLE [dbo].[Contracts] (
    [Typefullname] nchar(100)  NOT NULL
);
GO

-- Creating table 'Operations'
CREATE TABLE [dbo].[Operations] (
    [Name] nchar(20)  NOT NULL,
    [ContractTypefullname] nchar(100)  NOT NULL
);
GO

-- Creating table 'Subscribers'
CREATE TABLE [dbo].[Subscribers] (
    [Address] nchar(200)  NOT NULL,
    [OperationName] nchar(20)  NOT NULL,
    [OperationContractTypefullname] nchar(100)  NOT NULL,
    [ServiceTypefullname] nchar(100)  NOT NULL,
    [WantsToReturn] bit  NOT NULL
);
GO

-- Creating table 'Services'
CREATE TABLE [dbo].[Services] (
    [Typefullname] nchar(100)  NOT NULL
);
GO

-- Creating table 'ServiceContractAssocs'
CREATE TABLE [dbo].[ServiceContractAssocs] (
    [ServiceTypefullname] nchar(100)  NOT NULL,
    [ContractTypefullname] nchar(100)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Typefullname] in table 'Contracts'
ALTER TABLE [dbo].[Contracts]
ADD CONSTRAINT [PK_Contracts]
    PRIMARY KEY CLUSTERED ([Typefullname] ASC);
GO

-- Creating primary key on [Name], [ContractTypefullname] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [PK_Operations]
    PRIMARY KEY CLUSTERED ([Name], [ContractTypefullname] ASC);
GO

-- Creating primary key on [Address], [OperationName], [OperationContractTypefullname], [ServiceTypefullname] in table 'Subscribers'
ALTER TABLE [dbo].[Subscribers]
ADD CONSTRAINT [PK_Subscribers]
    PRIMARY KEY CLUSTERED ([Address], [OperationName], [OperationContractTypefullname], [ServiceTypefullname] ASC);
GO

-- Creating primary key on [Typefullname] in table 'Services'
ALTER TABLE [dbo].[Services]
ADD CONSTRAINT [PK_Services]
    PRIMARY KEY CLUSTERED ([Typefullname] ASC);
GO

-- Creating primary key on [ServiceTypefullname], [ContractTypefullname] in table 'ServiceContractAssocs'
ALTER TABLE [dbo].[ServiceContractAssocs]
ADD CONSTRAINT [PK_ServiceContractAssocs]
    PRIMARY KEY NONCLUSTERED ([ServiceTypefullname], [ContractTypefullname] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ServiceTypefullname] in table 'ServiceContractAssocs'
ALTER TABLE [dbo].[ServiceContractAssocs]
ADD CONSTRAINT [FK_ServiceServiceContractAssoc]
    FOREIGN KEY ([ServiceTypefullname])
    REFERENCES [dbo].[Services]
        ([Typefullname])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [ContractTypefullname] in table 'ServiceContractAssocs'
ALTER TABLE [dbo].[ServiceContractAssocs]
ADD CONSTRAINT [FK_ContractServiceContractAssoc]
    FOREIGN KEY ([ContractTypefullname])
    REFERENCES [dbo].[Contracts]
        ([Typefullname])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ContractServiceContractAssoc'
CREATE INDEX [IX_FK_ContractServiceContractAssoc]
ON [dbo].[ServiceContractAssocs]
    ([ContractTypefullname]);
GO

-- Creating foreign key on [ContractTypefullname] in table 'Operations'
ALTER TABLE [dbo].[Operations]
ADD CONSTRAINT [FK_ContractOperation]
    FOREIGN KEY ([ContractTypefullname])
    REFERENCES [dbo].[Contracts]
        ([Typefullname])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ContractOperation'
CREATE INDEX [IX_FK_ContractOperation]
ON [dbo].[Operations]
    ([ContractTypefullname]);
GO

-- Creating foreign key on [OperationName], [OperationContractTypefullname] in table 'Subscribers'
ALTER TABLE [dbo].[Subscribers]
ADD CONSTRAINT [FK_OperationSubscriber]
    FOREIGN KEY ([OperationName], [OperationContractTypefullname])
    REFERENCES [dbo].[Operations]
        ([Name], [ContractTypefullname])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_OperationSubscriber'
CREATE INDEX [IX_FK_OperationSubscriber]
ON [dbo].[Subscribers]
    ([OperationName], [OperationContractTypefullname]);
GO

-- Creating foreign key on [ServiceTypefullname] in table 'Subscribers'
ALTER TABLE [dbo].[Subscribers]
ADD CONSTRAINT [FK_ServiceSubscriber]
    FOREIGN KEY ([ServiceTypefullname])
    REFERENCES [dbo].[Services]
        ([Typefullname])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ServiceSubscriber'
CREATE INDEX [IX_FK_ServiceSubscriber]
ON [dbo].[Subscribers]
    ([ServiceTypefullname]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------