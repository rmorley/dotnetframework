﻿/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Runtime.CompilerServices;


namespace net.compasspoint.delivery
{
    /// <summary>
    /// for logging
    /// </summary>
    class ProxyWithAddress<T> : IDisposable
    {
        public T Proxy { get; set; }
        public string Address { get; set; }

        public void Dispose()
        {
                if (Proxy is IClientChannel)
                {
                    try
                    { //Technique per: http://msdn.microsoft.com/en-us/library/ms734681%28v=vs.100%29.aspx
                        ((IClientChannel)Proxy).Close();
                    }
                    //About close when IClientChannel in faulted state: http://msdn.microsoft.com/en-us/library/system.servicemodel.communicationstate%28v=vs.100%29.aspx
                    //An object in the Faulted state is not closed and may be holding resources. The Abort method should be used to close an object that has faulted. If Close is called on an object in the Faulted state, a CommunicationObjectFaultedException is thrown because the object cannot be gracefully closed.
                    catch 
                    {
                        ((IClientChannel)Proxy).Abort();
                    }
                }
                else
                {
                    try
                    {
                        ((IDisposable)Proxy).Dispose();
                    }
                    catch (Exception) { }
                }
                Proxy = default(T);
        }
    }

    /// <summary>
    /// This Model accesses Subscribers using SubscribersRepository 
    /// (This is in contrast to subscribers which subscribe, unsubscribe, and 
    /// enumerate deliverers using SubscribersServiceLibrary. Note that enumerating deliverers, not currently implemented, would be accomplished by exposing 
    /// SubscribersModel via a DataService, which would require a new WCFServiceApplication project since only that project type can currently
    /// host a DataService.)
    /// 
    /// subscribers (proxies or adapters) that implement ILogMessageGroupInformable will be provided the same log message group id used by this class for its invocation.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DelivererModel<T> where T : class
    {

        private static string LOCALSUBSCRIBERS_ADDRESSPREFIX = "local://";
        private static string LOCALSUBSCRIBERS_ADDRESSASSEMBLYTOCLASSDELIMITER = "/";
        //private static string LOCALSUBSCRIBERS_RELATIVEDIRECTORY = "localsubscribers";
        //private static string LOCALSUBSCRIBERS_FILEPATHDELIMITER = "\\";
        /// <summary>
        /// WcfService instantiates DelivererModel instances, which causes this constructor to be called. This constructor then does the following
        /// to ensure it -- and its contract(s) -- are registered:
        ///     1) register its contract(s) (SubscribersRepository.registerContract()) using its ContractTypefullname and OperationNames properties
        ///     2) register itself as a registered service (SubscribersRepository.registerServiceForContract()) using its ServiceTypefullname and 
        ///     SupportsMoreThanOneSubscriber properties to determine SubscribersRepository.registerServiceForContract()'s params.
        /// </summary>
        public DelivererModel()
        {
            //FIXME: complete
        }

        private string ServiceTypefullname
        {
            get { return this.GetType().FullName; }
        }

        private bool SupportsMoreThanOneSubscriber
        {
            get
            {
                SubscriberInvocationModeEnum subscriberInvocationMode = this.getsubscriberInvocationMode();
                if ( (subscriberInvocationMode == SubscriberInvocationModeEnum.SingleOneWay) 
                    || (subscriberInvocationMode == SubscriberInvocationModeEnum.SingleWithReturn) )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        private SubscriberInvocationModeEnum getsubscriberInvocationMode()
        {
            object [] customAttributes = this.GetType().GetCustomAttributes(true);

            SubscriberInvocationModeEnum subscriberInvocationMode = SubscriberInvocationModeEnum.SequentialWithExceptions; //default

            foreach (object customAttribute in customAttributes)
            {
                if (customAttribute is SubscriberInvocationModeAttribute)
                {
                    subscriberInvocationMode = ((SubscriberInvocationModeAttribute)customAttribute).SubscriberInvocationMode;
                }
            }
            return subscriberInvocationMode;
        }

        private List<string> OperationNames()
        {
            MethodInfo[] methodInfos = typeof(T).GetMethods();

            List<string> methodNames = new List<string>();

            foreach (MethodInfo methodInfo in methodInfos)
            {
                methodNames.Add(methodInfo.Name);
            }
            return methodNames;
        }


        /// <summary>
        /// This method determines the calling method's name programmatically
        /// and then calls the same method on subscribers.
        /// </summary> 
        [MethodImpl(MethodImplOptions.NoInlining)]
        public object deliver(params object[] args)
        {
            GroupId logMessageGroupId = new GroupId();

            SubscriberInvocationModeEnum subscriberInvocationMode = this.getsubscriberInvocationMode();

            StackFrame stackFrame = new StackFrame(1); //param is number of frames up the stack to skip - skipping 1 (this method)
            string operationName = stackFrame.GetMethod().Name;

            //Logger.submitToLog(new DiagnosticsLogMessage("Name of method invoking deliver() is '" + operationName + "'", new ComponentIdentity(this.GetType().FullName), "deliver"));
            Logger.submitToLog(new TraceLogMessage("Name of method invoking deliver() is '" + operationName + "'", new ComponentIdentity(this.GetType().FullName), "deliver"));

            try
            {
                Logger.submitToLog(new GroupableDiagnosticsLogMessage(logMessageGroupId, "DELIVERY BEGIN: Delivering for operation=" + typeof(T).FullName + "." + operationName + " Invocation Mode: " + subscriberInvocationMode.ToString(), new ComponentIdentity(this.GetType().FullName), "deliverToSubscribers"));

                bool firstWantsToReturn;

                List<ProxyWithAddress<T>> subscribers = getSubscriberInterfaceReferencesOfOperation(logMessageGroupId, operationName, out firstWantsToReturn);

                if (subscriberInvocationMode == SubscriberInvocationModeEnum.SingleOneWay) //Can only have one subscriber. Don't return value or exceptions to caller.
                {
                    if (subscribers.Count > 1)
                    {
                        string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SingleOneWay yet more than one subscriber";
                        Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                        throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                    }
                    else if (subscribers.Count == 1)
                    {
                        if (firstWantsToReturn)
                        {
                            string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SingleOneWay but single subscriber wants to return.";
                            Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                            throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                        }
                        else
                        {
                            try
                            {
                                invokeOperationOnSubscriber(logMessageGroupId, subscribers[0], operationName, args);
                            }
                            catch (Exception)
                            {//don't allow exception to be thrown back to caller. 
                                //Logger.submitToLog(new ErrorLogMessage("DELIVERY EXCEPTION: SingleOneWay invocation of operation " + operationName + " on address " + subscribers[0].Address + " threw the following exception: " + ex.ToString(), new ComponentIdentity(this.GetType().FullName), "deliver()"));
                            }
                        }
                    }
                    //if no subscribers, don't do anything -- caller shouldn't care.
                    return null;
                }
                else if (subscriberInvocationMode == SubscriberInvocationModeEnum.SingleWithReturn) // return values and exceptions to caller.
                {
                    if (subscribers.Count > 1)
                    {
                        string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SingleWithReturn yet more than one subscriber.";
                        Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                        throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                    }
                    else if (subscribers.Count == 1)
                    {
                        if (false)//!firstWantsToReturn) //don't really need to check: if doesn't want to return, it's OK to not return anything...
                        {
                            string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SingleWithReturn yet single subscriber doesn't want to return.";
                            Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                            throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                        }
                        else
                        {
                            return invokeOperationOnSubscriber(logMessageGroupId, subscribers[0], operationName, args);
                            // send all exceptions to caller
                        }
                    }
                    else
                    {
                        string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SingleWithReturn yet no subscribers.";
                        Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                        throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                    }
                }
                else if (subscriberInvocationMode == SubscriberInvocationModeEnum.SequentialOneWay) //for each subscriber in subscribers invoke sequentially, no return params, don't throw exceptions back to caller, and continue.
                {
                    if (subscribers.Count > 0)
                    {
                        if (firstWantsToReturn)
                        {
                            string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SequentialOneWay but subscriber wants to return.";
                            Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                            throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                        }
                        else
                        {
                            foreach (ProxyWithAddress<T> subscriber in subscribers)
                            {
                                try
                                {
                                    invokeOperationOnSubscriber(logMessageGroupId, subscriber, operationName, args);
                                }
                                catch (Exception)
                                {//don't allow exception to be thrown back to caller.
                                    //Logger.submitToLog(new ErrorLogMessage("SequentialOneWay invocation of operation " + operationName + " on address " + subscriber.Address + " threw the following exception: " + ex.ToString(), new ComponentIdentity(this.GetType().FullName), "deliver()"));
                                }
                            }
                        }
                    }
                    return null;
                }
                else if (subscriberInvocationMode == SubscriberInvocationModeEnum.SequentialWithExceptions) // for each subscriber in subscribers invoke sequentially, no return params, throw exceptions to caller and break.
                {
                    if (subscribers.Count > 0)
                    {
                        if (firstWantsToReturn)
                        {
                            string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SequentialWithExceptions but subscriber wants to return.";
                            Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                            throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                        }
                        else
                        {
                            foreach (ProxyWithAddress<T> subscriber in subscribers)
                            {
                                invokeOperationOnSubscriber(logMessageGroupId, subscriber, operationName, args);
                                //let exceptions return to caller and break out of loop
                            }
                            return null;  //no return value returned even for !isOneWay: wouldn't know which return value to return anyway...
                        }
                    }
                    else
                    {
                        string message = "DELIVERY CONFIG EXCEPTION: SubscriberInvocationMode is SequentialWithExceptions but no subscribers.";
                        Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + message, new ComponentIdentity("DelivererModel"), "deliver"));
                        throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = message });
                    }
                }
                else // ParallelOneWay  or WantsToReturnThenParallel 
                {
                    object returnValue = null;
                    if (subscribers.Count > 0)
                    {
                        if ((subscriberInvocationMode == SubscriberInvocationModeEnum.WantsToReturnThenParallel) && firstWantsToReturn)
                        {
                            returnValue = invokeOperationOnSubscriber(logMessageGroupId, subscribers[0], operationName, args);
                            subscribers.RemoveAt(0);
                        }
                        //only continue to call parallel subscribers if first doesn't exception.

                        WaitCallback threadPoolCallback = delegate(object subscriber)
                        {
                            try
                            {
                                invokeOperationOnSubscriber(logMessageGroupId, (ProxyWithAddress<T>)subscriber, operationName, args);
                            }
                            catch (Exception)
                            { //don't allow exception to be thrown back to caller.
                                //Logger.submitToLog(new ErrorLogMessage("DELIVERY EXCEPTION: Parallel invocation of operation " + operationName + " on address " + ((ProxyWithAddress<T>)subscriber).Address + " on thread " + System.Threading.Thread.CurrentThread.ManagedThreadId + " threw the following exception: " + ex.ToString(), new ComponentIdentity(this.GetType().FullName), "deliver()"));
                            }
                        };

                        Action<ProxyWithAddress<T>> queueUpThreadPoolCallback = delegate(ProxyWithAddress<T> subscriber)
                        {
                            ThreadPool.QueueUserWorkItem(threadPoolCallback, subscriber);
                        };


                        //for each subscriber in subscribers, queue up thread pool callback method that invokes operation
                        Array.ForEach<ProxyWithAddress<T>>(subscribers.ToArray(), queueUpThreadPoolCallback);
                    }

                    return returnValue;
                }
            }
            catch (FaultException<FaultDetail> fe)
            {
                throw fe;
            }
            catch (Exception ex)
            {
                Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "DELIVERY EXCEPTION: " + ex.Message, new ComponentIdentity("DelivererModel"), "deliver"));
                throw new FaultException<FaultDetail>(new FaultDetail() { Type = FaultDetailType.Other, Message = ex.Message });
            }
            finally
            {
                Logger.submitToLog(new GroupableDiagnosticsLogMessage(logMessageGroupId, "DELIVERY END: Delivery for operation=" + typeof(T).FullName + "." + operationName, new ComponentIdentity(this.GetType().FullName), "deliverToSubscribers"));
            }
        }

        private static object invokeOperationOnSubscriber(GroupId logMessageGroupId, ProxyWithAddress<T> subscriber, string operationName, object[] args)
        {
            try
            {
                Logger.submitToLog(new GroupableDiagnosticsLogMessage(logMessageGroupId, "INVOKE BEGIN: Invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                if (subscriber.Proxy is ILogMessageGroupInformable)
                {
                    ((ILogMessageGroupInformable)subscriber.Proxy).setLogMessageGroupId(logMessageGroupId);
                }


                Type type = typeof(T);
                MethodInfo methodInfo = type.GetMethod(operationName);
                if (methodInfo == null)
                {
                    throw new Exception("type.GetMethod(" + operationName + ") on type " + type.FullName + " returned null indicating method was not found."); 
                }
                //using (ServiceSecurityContext.Current.WindowsIdentity.Impersonate())
                //{
                try
                {
                    object obj = methodInfo.Invoke(subscriber.Proxy, args);
                    Logger.submitToLog(new GroupableDiagnosticsLogMessage(logMessageGroupId, "INVOKE END SUCCESS: Successfully invoked subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                    return obj;
                }
                catch (TargetInvocationException tie)
                {
                    if (tie.InnerException != null)
                    {
                        throw tie.InnerException;
                    }
                    else
                    {
                        throw new Exception("TargetInvocationException was thrown but no inner exception indicating the cause. Bug in.NET.");
                    }
                }
                //}
            }
            catch (TimeoutException te)
            {
                Logger.submitToLog(new GroupableTimeoutErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + te.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                //Logger.submitToLog(new GroupableTimeoutErrorLogMessage(logMessageGroupId, te.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                throw new FaultException<FaultDetail>(new FaultDetail() { Message = te.Message, Type = FaultDetailType.Timeout });
            }
            catch (FaultException<FaultDetail> fd)
            {
                if (fd.Detail.Type == FaultDetailType.Communication)
                {
                    Logger.submitToLog(new GroupableCommunicationErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                    //Logger.submitToLog(new GroupableCommunicationErrorLogMessage(logMessageGroupId, fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                }
                else if (fd.Detail.Type == FaultDetailType.Data)
                {
                    Logger.submitToLog(new GroupableDataErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                    //Logger.submitToLog(new GroupableDataErrorLogMessage(logMessageGroupId, fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                }
                else if (fd.Detail.Type == FaultDetailType.Destination)
                {
                    Logger.submitToLog(new GroupableDestinationErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                    //Logger.submitToLog(new GroupableDestinationErrorLogMessage(logMessageGroupId, fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                }
                else if (fd.Detail.Type == FaultDetailType.Timeout)
                {
                    Logger.submitToLog(new GroupableTimeoutErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                    //Logger.submitToLog(new GroupableTimeoutErrorLogMessage(logMessageGroupId, fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                }
                else if (fd.Detail.Type == FaultDetailType.Other)
                {
                    Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                    //Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, fd.Detail.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                }
                throw fd;
            }
            catch (System.ServiceModel.FaultException fe)
            {
                Logger.submitToLog(new GroupableDestinationErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + fe.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                //Logger.submitToLog(new GroupableDestinationErrorLogMessage(logMessageGroupId, fe.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                throw new FaultException<FaultDetail>(new FaultDetail() { Message = fe.Message, Type = FaultDetailType.Destination });
            }
            catch (CommunicationException cex)
            {
                Logger.submitToLog(new GroupableCommunicationErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + cex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                //Logger.submitToLog(new GroupableCommunicationErrorLogMessage(logMessageGroupId, cex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                throw new FaultException<FaultDetail>(new FaultDetail() { Message = cex.Message, Type = FaultDetailType.Communication });
            }
            catch (DataException dex)
            {
                Logger.submitToLog(new GroupableDataErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + dex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                //Logger.submitToLog(new GroupableDataErrorLogMessage(logMessageGroupId, dex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                throw new FaultException<FaultDetail>(new FaultDetail() { Message = dex.Message, Type = FaultDetailType.Data });
            }
            catch (DestinationException dex)
            {
                Logger.submitToLog(new GroupableDestinationErrorLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; " + dex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                //Logger.submitToLog(new GroupableDestinationErrorLogMessage(logMessageGroupId, dex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                throw new FaultException<FaultDetail>(new FaultDetail() { Message = dex.Message, Type = FaultDetailType.Destination });
            }
            catch (Exception ex)
            {
                Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "INVOKE END EXCEPTION: Exception caught invoking subscriber= " + subscriber.Address + " operation= " + typeof(T).FullName + "." + operationName + "; Exception type: " + ex.GetType().FullName + "; Message: " + ex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                //Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "Exception type: " + ex.GetType().FullName + "; Message: " + ex.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                if (ex.InnerException != null)
                {
                    Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "Inner Exception type: " + ex.InnerException.GetType().FullName + "; Message: " + ex.InnerException.Message, new ComponentIdentity("DelivererModel"), "invokeOperationOnSubscriber"));
                }
                throw new FaultException<FaultDetail>(new FaultDetail() { Message = ex.Message, Type = FaultDetailType.Other });
            }
            finally
            {
                ((IDisposable)subscriber).Dispose();
            }
        }

        private List<ProxyWithAddress<T>> getSubscriberInterfaceReferencesOfOperation(GroupId logMessageGroupId, string operationName, out bool firstWantsToReturn)
        {
            string serviceTypefullname = this.GetType().FullName;
            string contractTypefullname = typeof(T).FullName;

            List<string> addressStrings = SubscribersRepository.getInstance().getAddressesOfSubscribersOfOperation(serviceTypefullname, contractTypefullname, operationName, out firstWantsToReturn);

            List<ProxyWithAddress<T>> subscribers = new List<ProxyWithAddress<T>>(addressStrings.Count);

            foreach (string addressString in addressStrings)
            {
                try
                {
                    Binding binding = this.getBindingFromAddress(addressString);
                    if (binding != null)
                    {
                        //EndpointIdentity endpointIdentity = EndpointIdentity.CreateUpnIdentity(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                        //AddressHeaderCollection addressHeaderCollection = new AddressHeaderCollection();
                        //T proxy = ChannelFactory<T>.CreateChannel(binding, new EndpointAddress(new Uri(addressString), endpointIdentity, addressHeaderCollection));
                        //ChannelFactory<T> channelFactory = new ChannelFactory<T>(binding, new EndpointAddress(addressString));
                        //channelFactory.Credentials.UserName.UserName = ServiceSecurityContext.Current.WindowsIdentity.Name;
                        //channelFactory.Credentials.UserName.Password = ServiceSecurityContext.Current.AuthorizationContext;

                        //channelFactory.Credentials.
                        //T proxy = channelFactory.CreateChannel();
                        T proxy = ChannelFactory<T>.CreateChannel(binding, new EndpointAddress(addressString));
                        ProxyWithAddress<T> proxyWithAddress = new ProxyWithAddress<T>() { Proxy = proxy, Address = addressString };
                        subscribers.Add(proxyWithAddress);
                    }
                    else
                    {
                        string addressWithoutPrefix = addressString.Substring(LOCALSUBSCRIBERS_ADDRESSPREFIX.Length);
                        //string assemblyRelativePath = LOCALSUBSCRIBERS_FILEPATHDELIMITER + LOCALSUBSCRIBERS_RELATIVEDIRECTORY + LOCALSUBSCRIBERS_FILEPATHDELIMITER + addressWithoutPrefix.Substring(0, addressWithoutPrefix.IndexOf(LOCALSUBSCRIBERS_ADDRESSASSEMBLYTOCLASSDELIMITER));
                        string fullyQualifiedClassName = addressWithoutPrefix.Substring(addressWithoutPrefix.IndexOf(LOCALSUBSCRIBERS_ADDRESSASSEMBLYTOCLASSDELIMITER) + LOCALSUBSCRIBERS_ADDRESSASSEMBLYTOCLASSDELIMITER.Length);

                        //string executingAssemblyLocation = Assembly.GetExecutingAssembly().Location;
                        //executingAssemblyLocation = executingAssemblyLocation.Substring(0, executingAssemblyLocation.LastIndexOf(LOCALSUBSCRIBERS_FILEPATHDELIMITER));
                        //string assemblyFullPath = executingAssemblyLocation + assemblyRelativePath;
                        //Assembly assembly = Assembly.LoadFile(assemblyFullPath);
                        string simpleAssemblyName = addressWithoutPrefix.Substring(0, addressWithoutPrefix.IndexOf(LOCALSUBSCRIBERS_ADDRESSASSEMBLYTOCLASSDELIMITER) - 4);
                        Assembly assembly = Assembly.Load(new AssemblyName() { Name = simpleAssemblyName});

                        object obj = assembly.CreateInstance(fullyQualifiedClassName);
                        if (obj == null)
                        {
                            throw new Exception("Failed to create instance of type '" + fullyQualifiedClassName + "' because it was not found in assembly loaded from simple name '" + simpleAssemblyName + "' .");
                        }

                        ProxyWithAddress<T> proxyWithAddress = new ProxyWithAddress<T>() { Proxy = (T)obj, Address = addressString };
 
                        subscribers.Add(proxyWithAddress);
                    }
                    Logger.submitToLog(new TraceLogMessage("Successfully instantiated subscriber proxy to " + addressString, new ComponentIdentity("DelivererModel"), "getSubscriberInterfaceReferencesOfOperation")); 
                }
                catch (Exception ex)
                {
                    Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, "Exception caught instantiating local subscriber address = " + addressString + " for invocation of operation= " + typeof(T).FullName + "." + operationName + ". Skipping and continuing.", new ComponentIdentity("DelivererModel"), "getSubscriberInterfaceReferencesOfOperation"));
                    Logger.submitToLog(new GroupableFaultLogMessage(logMessageGroupId, ex.Message, new ComponentIdentity("DelivererModel"), "getSubscriberInterfaceReferencesOfOperation"));
                    //don't rethrow to instantiate other subscribers.
                }
            }
            return subscribers;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="addressString"></param>
        /// <returns>Binding or null if address starts with LOCALSUBSCRIBERS_ADDRESSPREFIX</returns>
        private Binding getBindingFromAddress(string addressString)
        {
            if (addressString.StartsWith("http:") || addressString.StartsWith("https:"))
            {
                WSHttpBinding binding = new WSHttpBinding(SecurityMode.Message, true);
                binding.ReliableSession.Enabled = true;
                binding.TransactionFlow = true;
                return binding;
            }
            else if (addressString.StartsWith("net.tcp:"))
            {
                NetTcpBinding binding = new NetTcpBinding(SecurityMode.Message, true);
                binding.ReliableSession.Enabled = true;
                binding.TransactionFlow = true;
                return binding;
            }
            else if (addressString.StartsWith("net.pipe:"))
            {
                NetNamedPipeBinding binding = new NetNamedPipeBinding();
                binding.TransactionFlow = true;
                return binding;
            }
            else if (addressString.StartsWith("net.msmq:"))
            {
                NetMsmqBinding binding = new NetMsmqBinding();
                binding.Security.Mode = NetMsmqSecurityMode.None;
                return binding;
            }
            else if (addressString.StartsWith(LOCALSUBSCRIBERS_ADDRESSPREFIX))
            {
                return null;
            }
            else
            {
                throw new Exception("Binding unknown for addressstring: " + addressString);
            }
        }
    }
}
