﻿/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace net.compasspoint.delivery
{
    public enum SubscriberInvocationModeEnum
    {
        SingleOneWay,

        SingleWithReturn,

        /// <summary>
        /// Ordered subscriber invocation. Exceptions and return values not returned to caller and exceptions do not
        /// stop invocations on subsequent subscribers.
        /// </summary>
        SequentialOneWay,

        /// <summary>
        /// Ordered subscriber invocation. Return values not returned to caller. First exception
        /// encountered stops invocations on subsequent subscribers and is returned to caller.
        /// 
        /// Default if attribute not specified on service implementation.
        /// </summary>
        SequentialWithExceptions,

        /// <summary>
        /// Unordered subscriber invocation. Return values and exceptions not returned to caller and exceptions do not stop
        /// invocations on other subscribers.
        /// </summary>
        ParallelOneWay,

        /// <summary>
        /// Invokes a single WantsToReturn subscriber then either invokes others in parallel and returns value of WantsToReturn subscriber
        /// or throws exception from WantsToReturn subscriber and doesn't invoke others in parallel. Service can only have one WantsToReturn subscriber.
        /// </summary>
        WantsToReturnThenParallel
    }

    public class SubscriberInvocationModeAttribute : Attribute
    {
        public SubscriberInvocationModeEnum SubscriberInvocationMode;

        public SubscriberInvocationModeAttribute(SubscriberInvocationModeEnum subscriberInvocationMode)
        {
            this.SubscriberInvocationMode = subscriberInvocationMode;
        }
    }
}
