/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace net.compasspoint
{
    public class IdentifiableObject
    {
        public string Id { get; set; }

        public object Obj { get; set; }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            if (this.Id == null)
            {
                return false;
            }
            else
            {
                IdentifiableObject otherIdentifiableObject = (IdentifiableObject)obj;
                return this.Id.Equals(otherIdentifiableObject.Id);
            }
        }
    }

    public class AuditLog
    {
        private static string DELIMITER_FIELD = "#@#$";
        //private static string DELIMITER_ROW = "\n\r";
        private static string FILE_TEMP_SUFFIX = "___TEMP";

        protected string filePath;

        public AuditLog(string relativeFilePath)
        {
            string absoluteDirectory = null;
            try
            {
                absoluteDirectory = AppResources.getConfigurationString("AuditLog.AbsoluteDirectory");
                this.filePath = absoluteDirectory + relativeFilePath;
            }
            catch (ConfigStringNotFoundException)
            {
                this.filePath = relativeFilePath;
            }
        }

        public string FilePath
        {
            get { return this.filePath; }
        }

        /// <summary>
        /// Writes identifiableObject to the underlying file. Either identifiableObject will be completely written to file without any errors 
        ///  or the file will be left in the state it was in at the beginning of the call and the exception encountered thrown.
        /// </summary>
        /// <param name="identifiableObjects">IdentifiableObjects to write to file</param>
        /// <param name="replaceAll">true deletes all data in file before writing, false writes to end of file.</param>
        public void write(IdentifiableObject identifiableObject, bool replaceAll)
        {
            this.write(new List<IdentifiableObject>() { identifiableObject }, replaceAll);
        }


        /// <summary>
        /// Writes a list of identifiableObjects to the underlying file. Either identifiableObjects will be completely written to file without any errors 
        ///  or the file will be left in the state it was in at the beginning of the call and the exception encountered thrown.
        /// </summary>
        /// <param name="identifiableObjects">IdentifiableObjects to write to file</param>
        /// <param name="replaceAll">true deletes all data in file before writing, false writes to end of file.</param>
        public void write(List<IdentifiableObject> identifiableObjects, bool replaceAll)
        {
            try
            {
                try
                {
                    //make sure temp is deleted if it exists for some reason.
                    File.Delete(this.filePath + FILE_TEMP_SUFFIX); //MS Docs: "An exception is not thrown if the specified file does not exist."
                }
                catch (DirectoryNotFoundException) { }

                //copy file to temp if file exists and replaceAll == false
                if (!replaceAll)
                {
                    if ((identifiableObjects != null) && (identifiableObjects.Count > 0))
                    {
                        try
                        {
                            //copy contents of file to temp if file exists
                            File.Copy(this.filePath, this.filePath + FILE_TEMP_SUFFIX);
                        }
                        catch (FileNotFoundException) { }
                        catch (DirectoryNotFoundException) { }
                    }
                    else
                    {
                        // if there are no objects and replaceAll is false, do nothing.
                        return;
                    }
                }
                else if (replaceAll && ((identifiableObjects == null) || (identifiableObjects.Count == 0)))
                {
                    try
                    {
                        //delete the audit file and do nothing else.
                        File.Delete(this.filePath); //MS Docs: "An exception is not thrown if the specified file does not exist."
                        return;
                    }
                    catch (DirectoryNotFoundException) { }
                }

                //otherwise...

                //if directory doesn't exist, create it 
                string directoryPath = filePath.Substring(0, filePath.LastIndexOf(@"\"));
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                //write identifiableObjects to temp
                using (FileStream stream = new FileStream(filePath + FILE_TEMP_SUFFIX, FileMode.OpenOrCreate))
                {
                    if (stream.Length > 0)
                    {
                        stream.Seek(0, SeekOrigin.End);  //begin writing to the position after the end of the file if the file has data
                    }
                    else
                    {
                        stream.Seek(0, SeekOrigin.Begin);  //or begin writing at beginning of the file if is empty
                    }
                    using (StreamWriter writer = new StreamWriter(stream))
                    {
                        foreach (IdentifiableObject identifiableObject in identifiableObjects)
                        {
                            DataContractSerializer serializer = new DataContractSerializer(identifiableObject.Obj.GetType());

                            writer.Write(identifiableObject.Id + DELIMITER_FIELD + identifiableObject.Obj.GetType().AssemblyQualifiedName + DELIMITER_FIELD);
                            writer.Flush();
                            serializer.WriteObject(stream, identifiableObject.Obj);
                            //writer.Write(DELIMITER_ROW);
                            writer.WriteLine();
                        }
                    }
                }

                //now copy and overwrite file with temp.
                File.Copy(this.filePath + FILE_TEMP_SUFFIX, this.filePath, true); //copy that overwrites destination
            }
            finally
            {
                //delete temp no matter what
                File.Delete(this.filePath + FILE_TEMP_SUFFIX); //MS Docs: "An exception is not thrown if the specified file does not exist."
            }

            //result will either be a file with data correctly and completely written to it (without any errors) 
            //  or the file in the state it was in at the beginining of the call if any errors are encountered with the error exception thrown.
        }

        /// <summary>
        /// Deserealizes all identifiableObjects in the file and returns them in a list. Does not alter file contents.
        /// </summary>
        /// <returns>A list of deserialized objects.</returns>
        /// <exception cref="Exception">Thrown if error is encountered while deserializing data.</exception>
        public List<IdentifiableObject> readAll()
        {
            List<IdentifiableObject> returnIdentifiableObjects = new List<IdentifiableObject>();
            try
            {
                using (FileStream stream = new FileStream(filePath, FileMode.Open))
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        string line = null;
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] fields = line.Split(new string[] { DELIMITER_FIELD }, StringSplitOptions.None);
                            if ((fields.Length == 3)
                                && (fields[0] != null)
                                && (fields[1] != null)
                                && (fields[2] != null)
                                && !fields[2].Equals(""))
                            {
                                try
                                {
                                    Type fieldObjectType = Type.GetType(fields[1]);

                                    using (XmlDictionaryReader dictionaryReader = XmlDictionaryReader.CreateTextReader(System.Text.Encoding.UTF8.GetBytes(fields[2]), new XmlDictionaryReaderQuotas()))
                                    {
                                        DataContractSerializer serializer = new DataContractSerializer(fieldObjectType);

                                        object deserializedObject = serializer.ReadObject(dictionaryReader, true);
                                        returnIdentifiableObjects.Add(new IdentifiableObject() { Id = fields[0], Obj = deserializedObject });
                                    }
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                    //System.Console.Error.WriteLine("Id + " + fields[0] + " not read due to error: " + ex.ToString());
                                }
                            }
                        }
                    }
                }

            }
            catch (FileNotFoundException)
            {
            }
            return returnIdentifiableObjects;
        }
    }
}
