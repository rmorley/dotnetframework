/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Configuration;
using System.Reflection;
using System.ComponentModel;
using System.Runtime.Remoting;
using System.Collections;
using System.Diagnostics;

namespace net.compasspoint
{

	/// <summary>
	/// Base class of all loggers
	/// </summary>
	/// <remarks>
	/// Optional CONFIGURATION:
	/// Logger.loggerclassname. Default: net.compasspoint.DefaultLogger
	/// Logger.logmessageformatterclassname. Default: net.compasspoint.ProductionLogMessageFormatter
	/// </remarks>
	public abstract class Logger
	{
		protected static string DEFAULT_ASSEMBLY_NAME = "Framework";
		private static string LOG_ASSEMBLY_NAME; 

		protected static string LOG_MESSAGE_FORMATTER;
		private static string LOG_CLASS_NAME;

		private static String DEFAULT_LOG_CLASS_NAME = "net.compasspoint.DefaultLogger";
		private static String DEFAULT_LOG_MESSAGE_FORMATTER = "net.compasspoint.ProductionLogMessageFormatter";

		private static Logger loggerInstance = null;
		private static BaseLogMessageFormatter logMessageFormatter = null;

		//The static constructor initializes the class (not objects) by setting defaults
		static Logger()
		{
			try
			{
				LOG_ASSEMBLY_NAME = AppResources.getConfigurationString("Logger.loggerAssembly");
			}
			catch (Exception)
			{
				LOG_ASSEMBLY_NAME = DEFAULT_ASSEMBLY_NAME;
			}

			try
			{
				LOG_CLASS_NAME = AppResources.getConfigurationString("Logger.loggerclassname");
			}
			catch (Exception)
			{
				LOG_CLASS_NAME = DEFAULT_LOG_CLASS_NAME;
			}

			try
			{
				LOG_MESSAGE_FORMATTER = AppResources.getConfigurationString("Logger.logmessageformatterclassname");
			}
			catch (Exception)
			{
				LOG_MESSAGE_FORMATTER = DEFAULT_LOG_MESSAGE_FORMATTER;
			}

			Logger.getInstance();
		}

		/// <summary>
		/// Base class of all loggers.
		/// </summary>
		protected Logger()
		{
		}

		/// <summary>
		/// Gets THE Logger
		/// </summary>
		/// <returns>THE Logger</returns>
		 static protected Logger getInstance()
		{
			if (Logger.loggerInstance == null)
			{
				try
				{
					ObjectHandle obj = Activator.CreateInstance(LOG_ASSEMBLY_NAME, LOG_CLASS_NAME);
					Logger.loggerInstance = (Logger) obj.Unwrap();

/*					Type assemblyType = AssemblyTypes.getAssemblyType(LOG_CLASS_NAME);
					Logger.loggerInstance = (Logger)Activator.CreateInstance(assemblyType);
*/
					Type assemblyType = AssemblyTypes.getAssemblyType(LOG_MESSAGE_FORMATTER);
					Logger.logMessageFormatter = (BaseLogMessageFormatter)Activator.CreateInstance(assemblyType);

					Logger.loggerInstance.Start();

				}
				catch (Exception ex)
				{
                    //throw ex;
					throw new RuntimeException(ex.ToString(), new ComponentIdentity("Logger"), "getInstance");
				}
			}

			return Logger.loggerInstance;
		}

		/// <summary>
		/// Shut down the logger
		/// </summary>
		public static void shutdown()
		{
			if (Logger.loggerInstance != null)
			{
			
				Logger.loggerInstance.Stop();
				Logger.loggerInstance = null;
				Logger.logMessageFormatter = null;
			}
		}

		/// <summary>
		/// Submit a log message
		/// </summary>
		/// <param name="logMessage">The log message</param>
		public static void submitToLog(LogMessage logMessage)
		{
            //This is needed for cases where user code accidentally calls ShutDown before quitting the app
            if (Logger.logMessageFormatter == null)
            {
                Logger.getInstance();
            }
            
            string message = Logger.logMessageFormatter.formatLogMessage(logMessage);
			if (message != null)
			{
				Logger.loggerInstance.writeLogMessage(message, logMessage.GetType().FullName);
			}
		}


		/// <summary>
		/// Submit an AppException
		/// </summary>
		/// <param name="ttex">The AppException</param>
		public static void submitToLog(AppException ttex)
		{
			ExceptionLogMessage elm = new ExceptionLogMessage(ttex);
            
            //This is needed for cases where user code accidentally calls ShutDown before quitting the app
            if (Logger.logMessageFormatter == null)
            {
                Logger.getInstance();
            }

			string message = Logger.logMessageFormatter.formatLogMessage(elm);
			if (message != null)
			{
				Logger.loggerInstance.writeLogMessage(message, typeof(ExceptionLogMessage).FullName);
			}
		}

        [Conditional("FRAMEWORK_TRACE")]
        public static void submitToLog(TraceLogMessage traceLogMessage)
        {
            Logger.submitToLog((LogMessage)traceLogMessage);
        }

		public static void MoveLogs(string path, int attemptTimeoutMillis)
		{
			Logger.loggerInstance.Move(path, attemptTimeoutMillis);
		}

		public static void CopyLogs(string path, int attemptTimeoutMillis)
		{
			Logger.loggerInstance.Copy(path, attemptTimeoutMillis);
		}

		protected abstract void writeLogMessage(string message, string logMessageType);

		/// <summary>
		/// Called on derived classes at startup
		/// </summary>
		protected virtual void Start()
		{
		}

		/// <summary>
		/// Called on derived classes at shutdown
		/// </summary>
		protected virtual void Stop()
		{
		}

		protected virtual void Move(string path, int attemptTimeoutMillis)
		{
		}

		protected virtual void Copy(string path, int attemptTimeoutMillis)
		{
		}

		public static string getLogFieldDelimiter()
		{
			return Logger.logMessageFormatter.getLogFieldDelimiter();
		}

        public static string getLogMessageDelimiter()
        {
            return Logger.logMessageFormatter.getLogMessageDelimiter();
        }

        public static string getFieldDelimitedFieldNamesInOrder()
        {
            return Logger.logMessageFormatter.getFieldDelimitedFieldNamesInOrder();
        }
	}
}

