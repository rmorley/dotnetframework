/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Net.Sockets;


namespace net.compasspoint
{
	/// <summary>
	/// A message (BaseMessage) type, LogMessage is specifically the base class of
    /// all log messages.
	/// </summary>
	public class LogMessage : BaseMessage
	{		
		protected ComponentIdentity componentIdentity;

		protected string sourceMethodName;		
		protected string sourceHostName;	
		protected string stackTrace = "";
		
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="messageToken">Should be localizable token. If not, will pass string through</param>
        /// <param name="componentIdentity"></param>
        /// <param name="sourceMethodName"></param>
        public LogMessage(String messageToken, ComponentIdentity componentIdentity, string sourceMethodName) :
			base(messageToken)
		{
			this.componentIdentity = componentIdentity;
			this.sourceMethodName = sourceMethodName;			
			this.setAutomaticData();
		}

		protected LogMessage(
			FrameworkDate eventTime,
			ComponentIdentity componentIdentity,
			String sourceMethodName,
			String sourceHostName,
			String message,
			String stackTrace
			) : base (eventTime, message)
		{
			
			this.componentIdentity = componentIdentity;
			this.sourceMethodName = sourceMethodName;			
			this.sourceHostName = sourceHostName;			
			this.stackTrace = stackTrace;
		}

		protected LogMessage(
			string message,
			ComponentIdentity componentIdentity,
			string sourceMethodName,
			string stackTrace
			) : base (message)
		{
			
			this.componentIdentity = componentIdentity;
			this.sourceMethodName = sourceMethodName;			
			this.setAutomaticData();			
			this.stackTrace = stackTrace;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>The host name (DNS) of the machine where the message was created.</returns>
        //OBSOLETE: replaced with SourceHostName property.  Mike Routen 7/19/06
        //public string getSourceHostName()
        //{
        //    return this.sourceHostName;
        //}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>The class generating the message.</returns>
        //OBSOLETE: replaced with ComponentIdentity property.  Mike Routen 7/19/06
        //public ComponentIdentity getComponentIdentity()
        //{
        //    return this.componentIdentity;
        //}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>The method name generating the message</returns>
        //OBSOLETE: replaced with SourceMethodName property.  Mike Routen 7/19/06
        //public string getSourceMethodname()
        //{
        //    return this.sourceMethodName;
        //}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
        //OBSOLETE: replaced with StackTrace property.  Mike Routen 7/19/06
        //public string getStackTrace()
        //{
        //    return this.stackTrace;
        //}
		protected override void setAutomaticData()
		{
			base.setAutomaticData();
			try
            {
				this.sourceHostName = System.Net.Dns.GetHostName();
			}
			catch(Exception)
			{
				this.sourceHostName = "UNKNOWN_HOST_ADDRESS";
			}
        }
        #region Properties

        public string SourceHostName
        {
            get { return (sourceHostName); }
        }

        public ComponentIdentity Component_Identity
        {
            get { return (componentIdentity); }
        }

        public string SourceMethodName
        {
            get { return (sourceMethodName); }
        }

        public string StackTrace
        {
            get { return (stackTrace); }
        }

        #endregion

    }
}
