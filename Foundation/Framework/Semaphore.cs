/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;

namespace net.compasspoint
{
	/// <summary>
	/// A semaphore lock: supports both exclusive (mutex) and inexclusive (semaphore) locks, both with and without
	/// timeouts. Abstracts programmers' code from MS complex, confusing, and changing locking API.
	/// </summary>
	public class Semaphore
	{
		private static int LOCKSLEEPMILLIS = 100;

		private bool exclusiveLock;
		private int sharedLock;

		private System.Object criticalSection;

		/// <summary>
		/// 
		/// </summary>
		public Semaphore()
		{
			this.exclusiveLock = false;
			this.sharedLock = 0;
			this.criticalSection = new System.Object();
		}

		/// <summary>
		/// For trying indefinitely to obtain the single exclusive lock.
		/// </summary>
		public void lockExclusively()
		{
			//Logger.trace("Begin waiting indefinitely for exclusive lock at: " + System.DateTime.Now, "Semaphore", "exclusiveLock");
			while (true)
			{
				lock(criticalSection)
				{
					if (exclusiveLock == false && sharedLock == 0)
					{
						exclusiveLock = true;
						break;
					}
				}
				try
				{
					System.Threading.Thread.Sleep(LOCKSLEEPMILLIS);
				}
				catch (Exception)
				{
				}

			}
			//Logger.trace("Completed waiting indefinitely for exclusive lock at: " + System.DateTime.Now, "Semaphore", "exclusiveLock");
			return;
		}

		/// <summary>
		/// For trying a certain number of milliseconds to obtain the single exclusive lock.
		/// </summary>
		/// <param name="milliseconds"></param>
		/// <returns></returns>
		public bool lockExclusively(int milliseconds)
		{
			bool succeeded = false;

			long timeoutTicks = System.DateTime.Now.Ticks + (milliseconds * 10000); //ticks are in 100ns resolution in .NET since 0001 epoc
long testTicks = System.DateTime.Now.Ticks;
			//Logger.trace("Begin waiting for a maximum of: " + milliseconds + " to get exclusive lock at: " + System.DateTime.Now, "Semaphore", "exclusiveLock(milliseconds)");
			while (timeoutTicks > System.DateTime.Now.Ticks)
			{
				lock(criticalSection)
				{
					if (exclusiveLock == false && sharedLock == 0)
					{
						exclusiveLock = true;
						succeeded = true;
						break;
					}
				}
				try
				{
					System.Threading.Thread.Sleep(LOCKSLEEPMILLIS);
				}
				catch (Exception)
				{
				}

			}
			//Logger.trace("Completed waiting for a maximum of: " + milliseconds + " for exclusive lock at: " + System.DateTime.Now + "; Succeeded getting lock = " + succeeded + ". ", "Semaphore", "exclusiveLock(milliseconds)");
			return succeeded;
		}

		/// <summary>
		/// For releasing the single exclusive lock.
		/// </summary>
		/// <returns></returns
		public bool releaseExclusiveLock()
		{
			bool succeeded = false;

			lock (criticalSection)
			{
				if (exclusiveLock == true)
				{
					exclusiveLock = false;
					succeeded = true;
				}
			}
			//Logger.trace("Tried releasing exclusive lock at: " + System.DateTime.Now + "; Succeeded releasing = " + succeeded + " (if false, not previously locked). ", "Semaphore", "exclusiveRelease");
			return succeeded;
		}

		/// <summary>
		/// 
		/// </summary>
		public void lockNonexclusively()
		{
			//Logger.trace("Begin waiting indefinitely for shared lock at: " + System.DateTime.Now + ". ", "Semaphore", "sharedLock");
			while (true)
			{
				lock (criticalSection)
				{
					if (exclusiveLock == false)
					{
						sharedLock++;
						break;
					}
				}
				try
				{
					System.Threading.Thread.Sleep(LOCKSLEEPMILLIS);
				}
				catch (Exception)
				{
				}

			}
			//Logger.trace("Completed waiting indefinitely for shared lock at: " + System.DateTime.Now + ". ", "Semaphore", "sharedLock");
			return;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="milliseconds"></param>
		/// <returns></returns>
		public bool lockNonexclusively(int milliseconds)
		{
			bool succeeded = false;

			long timeoutTicks = System.DateTime.Now.Ticks + (milliseconds * 10000); //ticks are in 100ns resolution in .NET since 0001 epoc

			//Logger.trace("Begin waiting for a maximum of: " + milliseconds + " to get shared lock at: " + System.DateTime.Now, "Semaphore", "sharedLock(milliseconds)");

			while (timeoutTicks > System.DateTime.Now.Ticks)
			{
				lock (criticalSection)
				{
					if (exclusiveLock == false)
					{
						sharedLock++;
						succeeded = true;
						break;
					}
				}
				try
				{
					System.Threading.Thread.Sleep(LOCKSLEEPMILLIS);
				}
				catch (Exception)
				{
				}

			}
			//Logger.trace("Completed waiting for a maximum of: " + milliseconds + " for shared lock at: " + System.DateTime.Now + "; Succeeded getting lock = " + succeeded + ". ", "Semaphore", "sharedLock(milliseconds)");
			return succeeded;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public bool releaseNonexclusiveLock()
		{
			bool succeeded = false;

			lock (criticalSection)
			{
				if (sharedLock > 0)
				{
					sharedLock--;
					succeeded = true;
				}
			}
			//Logger.trace("Tried releasing shared lock at: " + System.DateTime.Now + "; Succeeded releasing = " + succeeded + " (if false, no locks on semaphore). ", "Semaphore", "sharedRelease");
			return succeeded;

		}
	}
}
