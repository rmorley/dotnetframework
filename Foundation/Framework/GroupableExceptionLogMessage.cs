/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Text;

namespace net.compasspoint
{
	/// <summary>
	/// Log message for exceptions
	/// </summary>
	public class GroupableExceptionLogMessage : ErrorLogMessage
	{		
		public GroupableExceptionLogMessage(GroupId groupId, AppException aex) : 
			base(
            aex.EventTime,
			aex.getComponentIdentity(), 
			aex.getSourceMethodname(),
			aex.getSourceHostName(),
			GroupableDiagnosticsLogMessage.GROUPID_TAG + groupId.Id + GroupableDiagnosticsLogMessage.GROUPID_DELIMITER + aex.GetType().FullName+ "-->" + aex.getMessage(), 
			aex.getOriginalStackTrace())
				{
				}

        public GroupableExceptionLogMessage(GroupId groupId, Exception ex, ComponentIdentity componentIdentity, string sourceMethodName)
            : 
			base(
            GroupableDiagnosticsLogMessage.GROUPID_TAG + groupId.Id + GroupableDiagnosticsLogMessage.GROUPID_DELIMITER + ex.Message, 
			componentIdentity,
			sourceMethodName,
			ex.StackTrace)
		{

            if (ex.Data != null && ex.Data.Count > 0)
            {
                StringBuilder messageText = new StringBuilder(this.message);

                messageText.Append(" (Data:");
                foreach (string key in ex.Data.Keys)
                {
                    messageText.Append(" ");
                    messageText.Append(key);
                    messageText.Append("[");
                    messageText.Append(ex.Data[key]);
                    messageText.Append("]");
                }
                messageText.Append(")");

                this.message = messageText.ToString();

                message = messageText.ToString();
            }
        }
				
	}
}
