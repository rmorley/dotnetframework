/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.IO;

namespace net.compasspoint
{
	/// <summary>
	/// A collection of useful utilities for file handling
	/// </summary>
	public class FileUtilities
	{
		/// <summary>
		/// This method tries really hard to create a path
		/// </summary>
		/// <param name="path">The path to create</param>
		/// <param name="tries">Number of time to attempt the operation</param>
		/// <param name="delay">Time in millies to delay between tries</param>
		public static void createDirectory(String path, int tries, int delay)
		{
			for(int i = 0; i < tries; i++)
			{
				Directory.CreateDirectory(path);
				if(Directory.Exists(path))
					return;
				System.Threading.Thread.Sleep(delay);
				Logger.submitToLog(new TraceLogMessage("Trying to create directory again...", new ComponentIdentity("FileUtilities"), "createDirectory"));
			}
			throw new Exception("Unable to create Directory");
		}

		/// <summary>
		/// This method tries really hard to delete a directory
		/// </summary>
		/// <param name="path">The path to delete</param>
		/// <param name="recursive">Whether or not to delete the whole tree</param>
		/// <param name="tries">Number of time to attempt the operation</param>
		/// <param name="delay">Time in millies to delay between tries</param>
		public static void deleteDirectory(String path, bool recursive, int tries, int delay)
		{
			for(int i = 0; i < tries; i++)
			{
				try
				{
					Directory.Delete(path, recursive);
				}
				catch(Exception)
				{
				}

				if(!Directory.Exists(path))
					return;
				System.Threading.Thread.Sleep(delay);
				Logger.submitToLog(new TraceLogMessage("Trying to delete directory again...", new ComponentIdentity("FileUtilities"), "deleteDirectory"));
			}
			throw new Exception("Unable to delete Directory");
		}

		/// <summary>
		/// This method tries really hard to delete a file
		/// </summary>
		/// <param name="path">The path to delete</param>
		/// <param name="tries">Number of time to attempt the operation</param>
		/// <param name="delay">Time in millies to delay between tries</param>
		public static void deleteFile(String path, int tries, int delay)
		{
			for(int i = 0; i < tries; i++)
			{
				try
				{
					File.Delete(path);
				}
				catch(Exception)
				{
				}

				if(!File.Exists(path))
					return;
				System.Threading.Thread.Sleep(delay);
				Logger.submitToLog(new TraceLogMessage("Trying to delete file again...", new ComponentIdentity("FileUtilities"), "deleteFile"));
			}
			throw new Exception("Unable to delete file");
		}

		/// <summary>
		/// This method tries really hard to copy a file
		/// </summary>
		/// <param name="src">The src file</param>
		/// <param name="dst">The dst file</param>
		/// <param name="overwrite">true to overwrite</param>
		/// <param name="tries">Number of times to attempt the operation</param>
		/// <param name="delay">Time in millies to delay between tries</param>
		public static void copyFile(String src, String dst, bool overwrite, int tries, int delay)
		{
			bool copied;
			for(int i = 0; i < tries; i++)
			{
				try
				{
					copied = true;
					File.Copy(src, dst, overwrite);
				}
				catch(Exception)
				{
					copied = false;
				}

				if(copied)
					return;
				System.Threading.Thread.Sleep(delay);
				Logger.submitToLog(new TraceLogMessage("Trying to copy file again...", new ComponentIdentity("FileUtilities"), "copyFile"));
			}
			throw new Exception("Unable to copy file");
		}

		/// <summary>
		/// This method tries really hard to copy a file and uses the source filename as the 
		/// destination filename.
		/// </summary>
		/// <param name="src">The src file</param>
		/// <param name="dst">The dst file</param>
		/// <param name="overwrite">true to overwrite</param>
		/// <param name="tries">Number of times to attempt the operation</param>
		/// <param name="delay">Time in millies to delay between tries</param>

		public static void copyFile2(String src, String dst, bool overwrite, int tries, int delay)
		{
			bool copied;
			for(int i = 0; i < tries; i++)
			{
				try
				{
					copied = true;
					File.Copy(src, dst + Path.DirectorySeparatorChar + Path.GetFileName(src), overwrite);
				}
				catch(Exception)
				{
					copied = false;
				}

				if(copied)
					return;
				System.Threading.Thread.Sleep(delay);
				Logger.submitToLog(new TraceLogMessage("Trying to copy file again...", new ComponentIdentity("FileUtilities"), "copyFile2"));
			}
			throw new Exception("Unable to copy file");
		}
	}
}
