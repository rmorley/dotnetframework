/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace net.compasspoint
{
    public class QueueEntry
    {
        public string Message { get; set; }

        public string LogMessageType { get; set; }
    }

    /// <summary>
    /// A Logger that queues up log messages and then writes them
    /// to a local log file on another thread so as to not
    /// have file IO impact business logic threads' performance. This is a
    /// production local logger as compared to DefaultLogger which is really
    /// only appropriate for pre-release testing.
    /// </summary>
    public class QueuedLogger : Logger,ISingleThreadService
    {
        SingleThreadWorker singleThreadWorker = null;

        private static int DEFAULT_THREAD_SHUTDOWN_MILLIS = 10000; //10 seconds
        private static int DEFAULT_REPEAT_INTERVAL_MILLIS = 300000; // 5 minutes
//        private static int DEFAULT_REPEAT_INTERVAL_MILLIS = 5000; // 5 seconds for testing
        private int threadShutdownMillis;
        private int repeatIntervalMillis;

        private string logFilename;
        private const string DEFAULT_LOG_FILENAME = "c:\\log.log";

        private List<QueueEntry> messageQueue;  //MUST STAY PRIVATE for thread safety

        private Semaphore messageQueueLock;


        public QueuedLogger()
        {
            this.init();
            try
            {
                this.threadShutdownMillis = Convert.ToInt32(AppResources.getConfigurationString("Queuedlogger.threadshutdownmillis"));
            }
            catch (Exception)
            {
                this.threadShutdownMillis = QueuedLogger.DEFAULT_THREAD_SHUTDOWN_MILLIS;
            }
            try
            {
                this.repeatIntervalMillis = Convert.ToInt32(AppResources.getConfigurationString("Queuedlogger.repeatintervalmillis"));
            }
            catch (Exception)
            {
                this.repeatIntervalMillis = QueuedLogger.DEFAULT_REPEAT_INTERVAL_MILLIS;
            }

            this.messageQueueLock = new Semaphore();
            this.messageQueue = new List<QueueEntry>();
        }

        protected override void writeLogMessage(string message, string logMessageType)
        {
            QueueEntry queueEntry = new QueueEntry() { Message = message, LogMessageType = logMessageType };
            this.writeToQueue(queueEntry);
        }


        protected override void Move(string path, int attemptTimeoutMillis)
        {
            //No functionality is currently defined
        }

        protected override void Copy(string path, int attemptTimeoutMillis)
        {
            //No functionality is currently defined
        }


        protected override void Start()
        {
            this.singleThreadWorker = new SingleThreadWorker(this.threadShutdownMillis, this.repeatIntervalMillis, this, new ComponentIdentity(this.GetType().FullName)); //"QueuedLocalLogServer"
            this.singleThreadWorker.Startup();

        }

        /// <summary>
        /// Called on derived classes at shutdown
        /// </summary>
        protected override void Stop()
        {
            if (this.singleThreadWorker != null)
            {
                this.singleThreadWorker.Shutdown();
                this.singleThreadWorker = null;
            }
        }

        protected virtual void init()
        {
            try
            {
                this.logFilename = AppResources.getConfigurationString("Defaultlogger.logpath");
            }
            catch (Exception)
            {
                this.logFilename = DEFAULT_LOG_FILENAME;
            }
        }

        public virtual void firstTimeWork()
        {
            string directoryPath = this.logFilename.Substring(0, this.logFilename.LastIndexOf(@"\"));
            if (!Directory.Exists(directoryPath))
            {
                FileUtilities.createDirectory(directoryPath, 5, 100);
            }
        }

        public virtual void repeatedWork()
        {
            this.write(this.pullOutMessageQueueContents());
        }

        public virtual void exitWork()
        {
            this.write(this.pullOutMessageQueueContents());
        }

        protected List<QueueEntry> pullOutMessageQueueContents()
        {
            List<QueueEntry> messageQueueCopy = null;

            try
            {
                this.messageQueueLock.lockExclusively();
                messageQueueCopy = new List<QueueEntry>(this.messageQueue);
                this.messageQueue.Clear();
            }
            finally
            {
                this.messageQueueLock.releaseExclusiveLock();
            }
            return messageQueueCopy;
        }

        protected virtual void write(List<QueueEntry> queueEntries) //return param in case deriving class wants to also process copy of log queue.
        {
            StreamWriter streamWriter = CreateLogStream(this.logFilename);

            foreach (QueueEntry queueEntry in queueEntries)
            {
                streamWriter.Write(queueEntry.Message);
            }

            streamWriter.Flush();
            streamWriter.Close();
        }

        private void writeToQueue(QueueEntry queueEntry)
        {

            try
            {
                this.messageQueueLock.lockExclusively();
                this.messageQueue.Add(queueEntry);
            }
            finally
            {
                this.messageQueueLock.releaseExclusiveLock();
            }
        }

        private System.IO.StreamWriter CreateLogStream(string path)
        {
            System.IO.StreamWriter stream = null;
            string directoryPath = path.Substring(0, path.LastIndexOf(@"\"));
            if (!Directory.Exists(directoryPath))
            {
                FileUtilities.createDirectory(directoryPath, 5, 100);
            }
            if (!File.Exists(path))
            {
                stream = File.CreateText(path);
            }
            else
            {
                stream = File.AppendText(path);
            }
            return stream;
        }
    }
}
