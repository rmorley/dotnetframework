/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Threading;
using System.Reflection;
using System.Runtime.Remoting;


namespace net.compasspoint
{
	/// <summary>
	///  an extensible implementation of a single thread with clean shutdown that 
    /// calls instances of ISingleThreadService implementing the thread's runtime behavior.
	/// </summary>
	public class SingleThreadWorker : IService
	{
		public  int THREAD_SHUTDOWN_TIMEOUT ;//= 20000; // 20 seconds
		public  int THREAD_LOOP_SLEEP ;//= 30000; // 30 seconds
		
		protected System.Diagnostics.EventLog eventLogger;
		protected Thread workerThread;
		protected bool on = false;
		protected ISingleThreadService singleThreadService;
		protected ComponentIdentity componentIdentity;
        
		public SingleThreadWorker(int threadShutdownTimeoutMillis, int threadLoopSleepMillis, ISingleThreadService singleThreadService, ComponentIdentity componentIdentity)
		{
			this.THREAD_SHUTDOWN_TIMEOUT = threadShutdownTimeoutMillis;
			this.THREAD_LOOP_SLEEP = threadLoopSleepMillis;
			this.singleThreadService = singleThreadService;
			this.componentIdentity = componentIdentity;
		}

		public void setLogger(System.Diagnostics.EventLog eventLogger)
		{
			this.eventLogger = eventLogger;
		}

		public string Startup()
		{
						
			this.workerThread = new Thread(new ThreadStart(run));
			
			lock(this) // lock for this.on
			{
				this.on = true;
			}
			this.workerThread.Start();
            
            if ((this.workerThread != null) /*&& (this.workerThread.ManagedThreadId != null)*/)
            {
                return this.workerThread.ManagedThreadId.ToString();
            }
            else if (this.componentIdentity != null)
            {
                return this.componentIdentity.ToString();
            }
            else
            {
                return "";
            }
		}

		public void Shutdown()
		{
			lock(this) // lock for this.on
			{
				this.on = false;
			}
			Logger.submitToLog(new TraceLogMessage("Attempting to interrupt thread within following number of milliseconds: " + THREAD_SHUTDOWN_TIMEOUT, this.componentIdentity, "singleThreadWorker::Shutdown"));
			this.workerThread.Interrupt();
			
			bool isTerminated;
			try
			{
				isTerminated = this.workerThread.Join(THREAD_SHUTDOWN_TIMEOUT); //Wait for THREAD_SHUTDOWN_TIMEOUT milliseconds for the Thread to end gracefully, or
			}
			catch (ThreadStateException)
			{
				isTerminated = true;
			}

//			if (this.workerThread.IsAlive) // kill the thread ugly if it doesn't end
			if (!isTerminated)
			{							 // voluntarily in allotted time.
				Logger.submitToLog(new ErrorLogMessage("Could not interrupt thread. Attempting thread abort within following number of milliseconds: " + THREAD_SHUTDOWN_TIMEOUT, this.componentIdentity, "singleThreadWorker::Shutdown"));
				this.workerThread.Abort();
				try
				{
					isTerminated = this.workerThread.Join(THREAD_SHUTDOWN_TIMEOUT); //Wait for THREAD_SHUTDOWN_TIMEOUT milliseconds for the Thread to end gracefully, or
				}
				catch (ThreadStateException)
				{
					isTerminated = true;
				}

//				if (this.workerThread.IsAlive) 
				if (!isTerminated)
				{
					Logger.submitToLog(new ErrorLogMessage("Could not interrupt or abort thread. POTENTIAL SHUTDOWN BUG", this.componentIdentity, "singleThreadWorker::Shutdown"));
				}
				else
				{
					Logger.submitToLog(new ErrorLogMessage("Successfully aborted thread", this.componentIdentity, "singleThreadWorker::Shutdown"));
				}
			}
			else
			{
				Logger.submitToLog(new TraceLogMessage("CLEAN THREAD SHUTDOWN: Successfully interrupted thread within following number of milliseconds: " + THREAD_SHUTDOWN_TIMEOUT, this.componentIdentity, "singleThreadWorker::Shutdown"));
			}
		}

		protected void run()
		{
            try
            {
                this.singleThreadService.firstTimeWork();
                while (true)
                {
                    this.singleThreadService.repeatedWork();
                    /*
                     * Sleep() to not utilize too much CPU.
                     * NOTE: in .NET, another thread can call
                     * ThisThread.Interrupt() to break this thread out
                     * of this Sleep()...
                     */
                    Thread.Sleep(THREAD_LOOP_SLEEP);
                    //check to see if thread should exit each iteration of the loop
                    lock (this) // lock for this.on
                    {
                        if (!this.on)
                        {
                            break;
                        }
                    }
                }
            }
            catch (ThreadInterruptedException)
            { // normal exception when thread interrupted from sleeping, waiting, joining, etc.
            }
            catch (ThreadAbortException)
            {
                //Normal exception when thread aborts. Not an error.
            }
            catch (AppException aex)
            {
                Logger.submitToLog(aex);
            }
            catch (Exception ex)
            {
                Logger.submitToLog(new ExceptionLogMessage(ex, new ComponentIdentity("SingleThreadWorker"), "run"));
            }
			finally
			{
				this.singleThreadService.exitWork();
			}
		}
	}
}
