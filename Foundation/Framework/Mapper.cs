﻿/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using System.Reflection;

namespace net.compasspoint
{
    public abstract class Mapper
    {
        protected static string FILEPATHDELIMITER = "\\";

        public static string getFullPath(string pathRelativeToExecutingAssemblyDirectory)
        {
            string executingAssemblyLocation = Assembly.GetExecutingAssembly().Location;
            string executingAssemblyDirectory = executingAssemblyLocation.Substring(0, executingAssemblyLocation.LastIndexOf(FILEPATHDELIMITER) + 1);
            return executingAssemblyDirectory + pathRelativeToExecutingAssemblyDirectory;
 
        }
       /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyMapRelativePath">The path (without leading '\') relative to the executing assembly to the csv file with first column the source property name, second the destination property name, and
        /// optionally a third with the path of the property's source values to destination values map. Rows not with 2 or 3 columns are ignored.</param>
        /// <returns>A dictionary of source property names mapped to destination Properties</returns>
        public static Dictionary<string, Property> getPropertyMap(string propertyMapRelativePath)
        {
            Dictionary<string, Property> propertyMap = new Dictionary<string, Property>();

            TextFieldParser parser = new TextFieldParser(getFullPath(propertyMapRelativePath));
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            while (!parser.EndOfData)
            {
                string[] currentRow = parser.ReadFields();
                if (currentRow.Length == 2)
                {
                    propertyMap.Add(currentRow[0].Trim(), new Property() { SourceName = currentRow[0].Trim(), DestinationName = currentRow[1].Trim(), ValuesMap = null });
                }
                else if (currentRow.Length == 3)
                {
                    propertyMap.Add(currentRow[0].Trim(), new Property() { SourceName = currentRow[0].Trim(), DestinationName = currentRow[1].Trim(), ValuesMap = getValueMap(currentRow[2].Trim()) });
                }
            }
            return propertyMap;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueMapRelativePath">The path relative to the executing assembly to the csv file with first column the source value and the second the destination value
        /// Rows not with 2 columns are ignored.</param>
        /// <returns>A dictionary of source values mapped to destination values</returns>
        public static Dictionary<string, string> getValueMap(string valueMapRelativePath)
        {
            Dictionary<string, string> valueMap = new Dictionary<string, string>();

            TextFieldParser parser = new TextFieldParser(getFullPath(valueMapRelativePath));
            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");

            while (!parser.EndOfData)
            {
                string[] currentRow = parser.ReadFields();
                if (currentRow.Length == 2)
                {
                    valueMap.Add(currentRow[0].Trim(), currentRow[1].Trim());
                }
            }
            return valueMap;
        }

        /// <summary>
        /// Maps the sourceEntities to destination types. If a source property is not found in the propertyMap or the source value is null, it is skipped. 
        /// Missing values in any valuesMaps and destination properties in destination type results in exceptions.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceEntities"></param>
        /// <param name="propertyMap"></param>
        /// <returns></returns>
        protected List<T> mapSourceEntitiesToDestinationType<T>(List<Dictionary<string, string>> sourceEntities, Dictionary<string, Property> propertyMap) where T : new()
        {
            List<T> destinationTypes = new List<T>();

            foreach (Dictionary<string, string> sourceEntity in sourceEntities)
            {
                T t = new T();
                foreach (KeyValuePair<string, string> propertyValuePair in sourceEntity)
                {
                    Property property;
                    try
                    {
                        property = propertyMap[propertyValuePair.Key];
                    }
                    catch (KeyNotFoundException)
                    {
                        continue;
                    }
                    string destinationPropertyName = property.DestinationName;
                    string destinationValue = null;
                    if (property.ValuesMap != null)
                    {
                        try
                        {
                            destinationValue = property.ValuesMap[propertyValuePair.Value];
                        }
                        catch (KeyNotFoundException)
                        {
                            throw new FaultException("value '" + propertyValuePair.Value + "' for property '" + propertyValuePair.Key + "' not found.", new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType()");
                        }
                    }
                    else
                    {
                        destinationValue = propertyValuePair.Value;
                        if (destinationValue == null)
                        {
                            continue;
                        }
                    }

                    PropertyInfo namedProperty = typeof(T).GetProperty(destinationPropertyName);
                    if (namedProperty != null)
                    {
                        Type propertyType = namedProperty.PropertyType;

                        try
                        {
                            if (propertyType.Equals(typeof(int)))
                            {
                                namedProperty.SetValue(t, int.Parse(destinationValue), null);
                            }
                            else if (propertyType.Equals(typeof(decimal)))
                            {
                                namedProperty.SetValue(t, decimal.Parse(destinationValue), null);
                            }
                            else if (propertyType.Equals(typeof(double)))
                            {
                                namedProperty.SetValue(t, double.Parse(destinationValue), null);
                            }
                            else if (propertyType.Equals(typeof(DateTime)))
                            {
                                namedProperty.SetValue(t, DateTime.ParseExact(destinationValue, "s", System.Globalization.CultureInfo.InvariantCulture), null);
                            }
                            else
                            {
                                namedProperty.SetValue(t, destinationValue, null);
                            }
                        }
                        catch (FormatException fex)
                        {
                            throw new FaultException("Destination property '" + namedProperty.Name + "' is not the same type as the destination value. Exception reports: " + fex.Message, new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType()");
                        }
                        catch (Exception ex)
                        {
                            throw new FaultException("Error setting destination property '" + namedProperty.Name + "'. Exception reports: " + ex.Message, new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType()");
                        }
                    }
                    else
                    {
                        throw new FaultException("Property named '" + destinationPropertyName + "' not found on type '" +  typeof(T).FullName + "'", new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType()");
                    }
                }
                destinationTypes.Add(t);
            }
            return destinationTypes;
        }

        public List<T> mapSourceEntitiesToDestinationType<S, T>(List<S> sourceEntities, Dictionary<string, Property> propertyMap) where T : new()
        {
            List<T> destinationTypes = new List<T>();

            foreach (S sourceEntity in sourceEntities)
            {
                T t = new T();
                PropertyInfo[] sourceProperties = sourceEntity.GetType().GetProperties();

                foreach (PropertyInfo sourceProperty in sourceProperties)
                {
                    string sourcePropertyName = sourceProperty.Name;
                    Property property;
                    try
                    {
                        property = propertyMap[sourcePropertyName];
                    }
                    catch (KeyNotFoundException)
                    {
                        continue;
                    }
                    string destinationPropertyName = property.DestinationName;
                    string destinationStringValue = null;
                    object destinationValue = null;

                    object sourceValue = sourceProperty.GetValue(sourceEntity, null);

                    if (property.ValuesMap != null)
                    {
                        try
                        {
                            destinationStringValue = property.ValuesMap[sourceValue.ToString()];
                        }
                        catch (KeyNotFoundException)
                        {
                            throw new FaultException("Values map for " + sourcePropertyName + " exists but mapped destination value for source value '" + sourceValue.ToString() + "' not found.", new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType<S,T>()");
                        }
                    }
                    else
                    {
                        destinationValue = sourceValue;
                        if (destinationValue == null)
                        {
                            continue;
                        }
                    }

                    PropertyInfo destinationPropertyInfo = typeof(T).GetProperty(destinationPropertyName);
                    if (destinationPropertyInfo != null)
                    {
                        Type destinationPropertyType = destinationPropertyInfo.PropertyType;
                        if (destinationPropertyType != sourceProperty.PropertyType)
                        {
                            throw new FaultException("Source property " + sourcePropertyName + " and destination property " + destinationPropertyName + " types do not match.", new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType<S,T>()");
                        }

                        if (destinationValue != null)
                        {
                            destinationPropertyInfo.SetValue(t, destinationValue, null);
                        }
                        else
                        {
                            try
                            {
                                if (destinationPropertyType.Equals(typeof(int)))
                                {
                                    destinationPropertyInfo.SetValue(t, int.Parse(destinationStringValue), null);
                                }
                                else if (destinationPropertyType.Equals(typeof(decimal)))
                                {
                                    destinationPropertyInfo.SetValue(t, decimal.Parse(destinationStringValue), null);
                                }
                                else if (destinationPropertyType.Equals(typeof(double)))
                                {
                                    destinationPropertyInfo.SetValue(t, double.Parse(destinationStringValue), null);
                                }
                                else
                                {
                                    destinationPropertyInfo.SetValue(t, destinationValue, null);
                                }
                            }
                            catch (FormatException fex)
                            {
                                throw new FaultException("Destination property '" + destinationPropertyInfo.Name + "' cannot be converted into the destination value type. Exception reports: " + fex.Message, new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType<S,T>()");
                            }
                            catch (TargetException te)
                            {
                                throw new FaultException("Destination property '" + destinationPropertyInfo.Name + "' is not the same type as the destination value. Exception reports: " + te.Message, new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType<S,T>()");
                            }
                            catch (Exception ex)
                            {
                                throw new FaultException("Error setting destination property '" + destinationPropertyInfo.Name + "'. Exception reports: " + ex.Message, new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType<S,T>()");
                            }
                        }
                    }
                    else
                    {
                        throw new FaultException("Property named '" + destinationPropertyName + "' not found on type '" + typeof(T).FullName + "'", new ComponentIdentity(this.GetType().FullName), "mapSourceEntitiesToDestinationType<S,T>()");
                    }
                }
                destinationTypes.Add(t);
            }
            return destinationTypes;
        }

        protected abstract List<Dictionary<string, string>> getSourceEntities(Stream sourceStream);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceStream"></param>
        /// <param name="propertyMapPath">>The path relative to the executing assembly to csv file with first column the source property name, second the destination property name, and
        /// optionally a third with the path of the property's source values to destination values map.</param>
        /// <returns></returns>
        public List<T> getDestinationEntities<T>(Stream sourceStream, string propertyMapRelativePath) where T : new()
        {
            return mapSourceEntitiesToDestinationType<T>(getSourceEntities(sourceStream), getPropertyMap(propertyMapRelativePath));
        }
    }

    public class Property
    {
        /// <summary>
        /// Source property's name.
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        /// Destination property's name
        /// </summary>
        public string DestinationName { get; set; }
     
        /// <summary>
        /// A dictionary of source values mapped to destination values for this property.
        /// </summary>
        public Dictionary<string, string> ValuesMap { get; set; }
    }
}
