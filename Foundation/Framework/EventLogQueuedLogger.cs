﻿/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace net.compasspoint
{
    public class EventLogQueuedLogger : QueuedLogger
    {
        private string source = null;
        
        protected override void init()
        {
            try
            {
                source = AppResources.getConfigurationString("Service.eventsourcestring");
            }
            catch (ConfigStringNotFoundException) { }

            if (!System.Diagnostics.EventLog.SourceExists(source))
                System.Diagnostics.EventLog.CreateEventSource(source, "Application");

        }

        public override void firstTimeWork()
        {
            if ( (source != null) && !System.Diagnostics.EventLog.SourceExists(source))
                System.Diagnostics.EventLog.CreateEventSource(source, "Application");
        }
       
        protected override void write(List<QueueEntry> queueEntries) //return param in case deriving class wants to also process copy of log queue.
        {
            if (source != null)
            {
                foreach (QueueEntry queueEntry in queueEntries)
                {
                    if (queueEntry.LogMessageType.Equals(typeof(ErrorLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 0);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(ExceptionLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 0);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(FaultExceptionLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 1);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(GroupableFaultLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 1);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(GroupableExceptionLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 0);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(GroupableErrorLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 0);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(GroupableCommunicationErrorLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 2);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(GroupableDataErrorLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 3);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(GroupableDestinationErrorLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 4);
                    }
                    else if (queueEntry.LogMessageType.Equals(typeof(GroupableTimeoutErrorLogMessage).FullName))
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Error, 5);
                    }
                    else
                    {
                        EventLog.WriteEntry(source, queueEntry.Message, EventLogEntryType.Information);
                    }
                }
            }
        }
    }
}
