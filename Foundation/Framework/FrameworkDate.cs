/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Globalization;

namespace net.compasspoint
{
	/// <summary>
	/// Utilities for abstracting system/application dates from 
    /// Microsoft's changing date api.
	/// </summary>
	[Serializable]
	public class FrameworkDate
	{
		protected DateTime frameworkDate;
	
		/// <summary>
		/// Default Constructor
		/// </summary>
		public FrameworkDate()
		{
			this.frameworkDate = DateTime.Now;
		}

		/// <summary>
		/// Constructor for DateTime
		/// </summary>
		/// <param name="frameworkDate">DateTime</param>
		public FrameworkDate(DateTime frameworkDate)
		{
			this.frameworkDate = frameworkDate;
		}
	
		/// <summary>
		/// Constructor for a date string
		/// </summary>
		/// <param name="dateString">string</param>
		public FrameworkDate(String dateString) 
		{
			try
			{
				this.frameworkDate = DateTime.Parse(dateString);
			}
			catch(Exception ex)
			{
				throw new AppException(ex.Message, new ComponentIdentity("FrameworkDate"), "constructor(string)");
			}
		}
	
		/// <summary>
		/// 
		/// </summary>
		/// <returns>Gets the Date in the form of a DateTime object</returns>
		// OBSOLETE: replaced with Framework_Date property.  Mike Routen 71/19/06
        //public DateTime getFrameworkDate()
        //{
        //    return this.frameworkDate;
        //}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>Gets a string representing the DateTime object</returns>
		public string getSQLFrameworkDate()
		{			
			return this.frameworkDate.ToShortDateString() + " " + frameworkDate.ToLongTimeString();
		}

		/// <summary>
		/// Gets a string representing the UTC time
		/// </summary>
		/// <returns></returns>
		public string getSQLUTCFrameworkDate()
		{
			return this.frameworkDate.ToUniversalTime().ToShortDateString() + " " + frameworkDate.ToUniversalTime().ToLongTimeString();
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>Converts the frameworkdate into a string</returns>
		public override string ToString()
		{
			DateTime universalTime = this.frameworkDate.ToUniversalTime();

			return universalTime.ToShortDateString() + " " + universalTime.ToString("T", DateTimeFormatInfo.InvariantInfo);
        }

        #region Properties

        public DateTime Framework_Date
        {
            get { return (frameworkDate); }
        }

        #endregion

    }
}
