/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Reflection;
using System.Threading;
using System.Runtime.Remoting;

namespace net.compasspoint
{
	/// <summary>
	/// An implementation of IService that uses the console window.
    /// Primarily used by the Service assembly when it is not 
    /// configured to run as a Windows Service in its configuration file
    /// (<add key = "Service.isService" value = "false" />)
	/// </summary>
	public class ConsoleBootstrapper : IServiceEx
	{		
		protected IService service;

		public ConsoleBootstrapper()
		{
            string serviceClassName = AppResources.getConfigurationString("ServiceClassNameService");

            string serviceAssemblyName = AppResources.getConfigurationString("ServiceAssemblyName");
            ObjectHandle obj = Activator.CreateInstance(serviceAssemblyName, serviceClassName);
            service = (IService)obj.Unwrap();
		}


		public string Startup()
		{
            throw new NotImplementedException();
        }

		public void Shutdown()
		{
			this.service.Shutdown();	
//			Logger.shutdown();
		}

        #region IServiceEx Members

        public string Startup(string[] args)
        {
            try
            {
                string instanceId = "";// service.Startup();
                if (service is IServiceEx)
                {
                    instanceId = ((IServiceEx)service).Startup(args);
                }
                else
                {
                    instanceId = service.Startup();
                }



                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

                string assemblyStrings = "\n";

                foreach (Assembly assembly in assemblies)
                {
                    if (!assembly.GlobalAssemblyCache)
                    {
                        assemblyStrings += assembly.GetName().FullName + ";\n";
                    }
                }
                Console.WriteLine(String.Format(AppResources.getConfigurationString("Framework.strRunning"), "\n", assemblyStrings, instanceId));
                Console.ReadLine();
                return instanceId;
            }
            catch (UsageException uex)
            {
                Console.WriteLine(uex.Message);
                Console.ReadLine();
                throw uex;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadLine();
                throw ex;
            }
            finally
            {
                this.Shutdown();
            }
        }

        #endregion
    }
}
