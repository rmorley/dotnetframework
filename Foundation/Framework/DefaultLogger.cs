/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.IO;

namespace net.compasspoint
{
	/// <summary>
	/// A simple logger implementation that writes to a local log file
    /// in a blocking fashion.
	/// </summary>
	public class DefaultLogger : Logger
	{
		private string logFilename;
		private System.IO.StreamWriter logFilenameWriter;
		private Semaphore logFilenameWriterLock;

		private const string DEFAULT_LOG_FILENAME = "c:\\log.log";
		private int WRITE_ATTEMPT_TIMEOUT_MILLIS = 10000;
		private int WRITER_LOCK_TIMEOUT_MILLIS = 5000;


		public DefaultLogger()
		{
			try
			{
				this.logFilename = AppResources.getConfigurationString("defaultlogger.logpath");
			}
			catch (Exception )
			{
				this.logFilename = DEFAULT_LOG_FILENAME;
			}
			
			this.logFilenameWriterLock = new Semaphore();
		}

		protected System.IO.StreamWriter CreateLogStream(string path)
		{
			System.IO.StreamWriter stream = null;
			string directoryPath = path.Substring(0,path.LastIndexOf(@"\"));
			if (!Directory.Exists(directoryPath))
			{
				FileUtilities.createDirectory(directoryPath,5,100);
			}
			if (!File.Exists(path))
			{
				stream = File.CreateText(path);
			}
			else
			{
				stream = File.AppendText(path);
			}
			return stream;
		}

        protected override void writeLogMessage(string message, string logMessageType)
		{
			this.write(message, WRITE_ATTEMPT_TIMEOUT_MILLIS);
		}
		
		protected override void Move(string path, int attemptTimeoutMillis)
		{
		}
		
		protected override void Copy(string path, int attemptTimeoutMillis)
		{
		}

		private void write(string message, int attemptTimeoutMillis)
		{
			
			try
			{
				if (!this.logFilenameWriterLock.lockExclusively(attemptTimeoutMillis))
				{
					System.Console.Error.WriteLine("Couldn't obtain logWriterLock in DefaultLogger::write() within " + Convert.ToString(attemptTimeoutMillis / 1000) + " sec. Aborting Log write of message: " + message);
				}
				else
				{
					if (logFilenameWriter != null)
					{
						logFilenameWriter.Write(message);
                        logFilenameWriter.Flush();
					}
				}
			}
			finally
			{
				this.logFilenameWriterLock.releaseExclusiveLock();
			}
		}

		protected override void Start()
		{
			try
			{
				if (!this.logFilenameWriterLock.lockExclusively(WRITER_LOCK_TIMEOUT_MILLIS))
				{
					throw new CouldntObtainLockException(this.logFilename, new ComponentIdentity(this.GetType().FullName), "start");
				}
				if (this.logFilenameWriter == null)
				{
					this.logFilenameWriter = CreateLogStream(this.logFilename);
				}
			}
			catch (AppException aex)
			{
				throw aex;
			}
			catch (Exception ex)
			{
				throw new AppException(ex, new ComponentIdentity(this.GetType().FullName), "start");
			}
			finally
			{
				this.logFilenameWriterLock.releaseExclusiveLock();
			}
		}

		/// <summary>
		/// Called on derived classes at shutdown
		/// </summary>
		protected override void Stop()
		{
			try
			{
				this.logFilenameWriterLock.lockExclusively(WRITER_LOCK_TIMEOUT_MILLIS);
				if (this.logFilenameWriter != null)
				{
					this.logFilenameWriter.Flush();
					this.logFilenameWriter.Close();
					this.logFilenameWriter = null;
				}
			}
			catch (Exception)
			{
			}
			finally
			{
				this.logFilenameWriterLock.releaseExclusiveLock();
			}
		}
	}
}
