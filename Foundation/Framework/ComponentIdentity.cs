/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;

namespace net.compasspoint
{
	/// <summary>
	/// The Identity of a Component. Used to address Component and for
    /// constructing a remoting name.
	/// </summary>
	[Serializable]
	public class ComponentIdentity
	{
		private string componentName;

		private string sourceClassName;

		public ComponentIdentity()
		{
			this.componentName = "";
			this.sourceClassName = "";
		}
		
		public ComponentIdentity(string componentName, string sourceClassName)
		{
			this.componentName = componentName;
			this.sourceClassName = sourceClassName;
		}

		public string ComponentName
		{
			get { return componentName; }
			set { componentName = value; }
		}

		public string SourceClassName
		{
			get { return sourceClassName; }
			set { sourceClassName = value; }
		}

		public override bool Equals(object obj)
		{
			if ((obj is ComponentIdentity) 
				&& ((ComponentIdentity) obj).ComponentName.Equals(this.ComponentName))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		public override string ToString()
		{
			return componentName + ": " + this.sourceClassName;
		}
		
		public void clear()
		{
			this.componentName = "";
		}

		public ComponentIdentity(string sourceClassName)
		{
			this.componentName = "";
			this.sourceClassName = sourceClassName;
		}

	}
}
