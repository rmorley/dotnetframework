/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Reflection;
using System.Resources;

namespace net.compasspoint
{
	/// <summary>
	/// Utility class for accessing localized strings for Internationalization support as 
	/// well as application configuration settings. Abstracts programmers' code
    /// from changing MS framework methods for configuration file access and simplifies
    /// pulling strings out of code without learning/trying to remember MS resource methods.
    /// </summary>
	public class AppResources
	{
        protected static ResourceManager resourceManager = null;

        static AppResources()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();

            if (entryAssembly != null)
            {
                resourceManager = new ResourceManager("net.compasspoint.Properties.Resources", entryAssembly);
            }
        }

		protected AppResources()
		{
		}

		/// <param name="token">An internationalizable token to be localized</param>
		/// <returns>localized string if token exists as a key in the resources or the token itself unaltered if it doesn't exist as a key in the resources</returns>
		public static string getLocalizedString(string token)
		{
            string localizedString;
            try
            {
                localizedString = resourceManager.GetString(token);
            }
            catch (Exception)
            {
                localizedString = token;
            }
            //localizedString = System.Configuration.ConfigurationManager.AppSettings[token];// System.Configuration.ConfigurationSettings.AppSettings[token];
			if (localizedString == null)
			{
				return token;
			}
			else
			{
				return localizedString;
			}
		}

		/// <param name="token">An internationalizable token to be localized</param>
		/// <param name="defaultString">The string to return in the case that the token doesn't exist</param>
		/// <returns>localized string if token exists as a key in the resources or the defaultString if token does not exist as a key in the resources</returns>
		public static string getLocalizedString(string token, string defaultString)
		{
			string localizedString;
			try
			{
                localizedString = resourceManager.GetString(token);

                //localizedString = System.Configuration.ConfigurationManager.AppSettings[token];//System.Configuration.ConfigurationSettings.AppSettings[token];
			}
			catch (Exception)
			{
				localizedString = defaultString;
			}
			if ((localizedString == null) || (localizedString.Equals("")))
				localizedString = defaultString;

			return localizedString;
		}

		/// <param name="configToken"></param>
		/// <returns>configuration string</returns>
		/// <exception cref="ConfigStringNotFoundException">if key isn't found</exception>
		public static string getConfigurationString(string configToken) // throws ConfigStringNotFoundException
		{
				string confString;
                confString = System.Configuration.ConfigurationManager.AppSettings[configToken];//System.Configuration.ConfigurationSettings.AppSettings[configToken];
				if (confString == null)//AppSettings : NameValueCollection::Get() returns null if key not found.
				{
					//string retMessage = "Configuration string cooresponding to config token " + configToken + " not found in configuration resources.";
					//if key not found, throw exception.
					throw new ConfigStringNotFoundException(configToken + " not found in configuration file.", new ComponentIdentity("Resources"), "getConfigurationString");
				}
				else
				{
					return confString;
				}
		}

        /// <param name="configToken"></param>
        /// <returns>database connection string</returns>
        /// <exception cref="ConfigStringNotFoundException">if key isn't found</exception>
        public static string getDatabaseConnectionString(string configToken)
        {
            string confString;
            confString = System.Configuration.ConfigurationManager.ConnectionStrings[configToken].ToString();
            if (confString == null)
            {
                string retMessage = "Connection string corresponding to config token " + configToken + " not found in configuration resources.";

                throw new ConfigStringNotFoundException(configToken + " not found in configuration file.", new ComponentIdentity("Resources"), "getDatabaseConnectionString");

            }
            else
            {
                return confString;
            }
        }
	}
}
