/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Reflection;
using System.IO;
using System.Collections;

namespace net.compasspoint
{
	/// <summary>
	///  Class for managing the assembly names in the current working directory
	/// </summary>
	public class AssemblyTypes
	{
		private static string EXE = ".exe";
		private static string DLL = ".dll";

		/// <summary>
		/// Searches all assemblies in the current working directory for the specified className
		/// </summary>
		/// <param name="className">Name of the class</param>
		/// <returns>The Assembly Name</returns>
		public static Type getAssemblyType(string className)
		{
			ArrayList assemblies = new ArrayList();
			Type retVal = null;

			retVal = Type.GetType(className);
			if(retVal == null)
			{
				//get the list of files in the current directory
				string[] fileEntries = Directory.GetFiles(
					Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
					);

				//we only want files with .exe or .dll extensions
				foreach (string fileName in fileEntries)
				{
					if( (Path.GetExtension(fileName).ToLower().Equals(EXE)) || (Path.GetExtension(fileName).ToLower().Equals(DLL)) ) 
						assemblies.Add(Path.GetFileNameWithoutExtension(fileName));
				}
				
				//if the full assembly name matches...
				foreach (string s in assemblies)
					if (Type.GetType(className + "," + s) != null)
						retVal = Type.GetType(className + "," + s);
			}

			return retVal;
		}
	}
}
