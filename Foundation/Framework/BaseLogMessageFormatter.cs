/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;

namespace net.compasspoint
{
	/// <summary>
	/// The base class log message formatter, which takes log message
    /// data and delimits it for log persistence.
	/// </summary>
	public class BaseLogMessageFormatter
	{
		private static string FIELD_DELIMITER = @"; ";
        private static string LOG_MESSAGE_DELILMITER = "\r\n";
        
        protected  const string EVENTTIME         = "EventTime";
        protected  const string MESSAGETYPE       = "MessageType";
        protected  const string MESSAGE           = "Message";
        protected  const string SOURCEHOSTNAME    = "SourceHostName";
        protected  const string COMPONENTNAME     = "ComponentName";
        protected  const string SOURCECLASSNAME   = "SourceClassName";
        protected  const string SOURCEMETHODNAME  = "SourceMethodName";
        protected  const string STACKTRACE        = "StackTrace";

        
        /// <summary>
		/// Get the log message field (column) delimiter
		/// </summary>
		/// <returns>Field Delimiter</returns>
		public String getLogFieldDelimiter()
		{
			return FIELD_DELIMITER;
		}

        /// <summary>
        /// Gets the log message (row) delimiter
        /// </summary>
        /// <returns></returns>
        public String getLogMessageDelimiter()
        {
            return LOG_MESSAGE_DELILMITER;
        }

        public String getFieldDelimitedFieldNamesInOrder()
        {
            return
                EVENTTIME           + FIELD_DELIMITER +
                MESSAGETYPE         + FIELD_DELIMITER +
                MESSAGE             + FIELD_DELIMITER +
                SOURCEHOSTNAME      + FIELD_DELIMITER +
                COMPONENTNAME       + FIELD_DELIMITER +
                SOURCECLASSNAME     + FIELD_DELIMITER +
                SOURCEMETHODNAME    + FIELD_DELIMITER +
                STACKTRACE          + FIELD_DELIMITER;
        }

		public virtual String formatLogMessage (LogMessage logMessage)
		{
            return
                // OBSOLETE: replaced with call to EventTime property.  Mike Routen 7/19/06
                //RemoveCRLF(logMessage.getEventTime().ToString()) + FIELD_DELIMITER +
                RemoveCRLF(logMessage.EventTime.ToString()) + FIELD_DELIMITER +
                RemoveCRLF(logMessage.GetType().FullName) + FIELD_DELIMITER +
                RemoveCRLF(logMessage.ToString()) + FIELD_DELIMITER +
                //OBSOLETE: replaced with call to SourceHostName property.  Mike Routen 7/19/06
                //RemoveCRLF(logMessage.getSourceHostName()) + FIELD_DELIMITER +
                RemoveCRLF(logMessage.SourceHostName) + FIELD_DELIMITER +
                //OBSOLETE: replaced with call to ComponentIdentity property.  Mike Routen 7/19/06
                //RemoveCRLF(logMessage.getComponentIdentity().ComponentName) + FIELD_DELIMITER +
                RemoveCRLF(logMessage.Component_Identity.ComponentName) + FIELD_DELIMITER +
                //OBSOLETE: replaced with call to ComponentIdentity property.  Mike Routen 7/19/06
                //RemoveCRLF(logMessage.getComponentIdentity().SourceClassName) + FIELD_DELIMITER +
                RemoveCRLF(logMessage.Component_Identity.SourceClassName) + FIELD_DELIMITER +
                //OBSOLETE: replaced with call to SourceMethodname property.  Mike Routen 7/19/06
                //RemoveCRLF(logMessage.getSourceMethodname()) + FIELD_DELIMITER +
                RemoveCRLF(logMessage.SourceMethodName) + FIELD_DELIMITER +
                //OBSOLETE: replaced with call to StackTrace property.  Mike Routen 7/19/06
                //RemoveCRLF(logMessage.getStackTrace()) + FIELD_DELIMITER;
                RemoveCRLF(logMessage.StackTrace) + FIELD_DELIMITER + LOG_MESSAGE_DELILMITER;
        }

		private string RemoveCRLF(string message)
		{
			if (message != null && message.Length > 0)
                return message.Replace(LOG_MESSAGE_DELILMITER, " ").Replace("\n", " ").Replace(FIELD_DELIMITER, ":");
			else
				return message;
		}

	}
}
