/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Runtime.Serialization;

namespace net.compasspoint
{
	/// <summary>
	/// Base framework exception. This exception enhances .NET exceptions to include source
	/// timestamps, host name, etc., and is also designed to integrate with the Logger.
	/// This class can be used as base class for both larger distributed and smaller application
	/// custom exceptions.
	/// </summary>
	[Serializable]
	public class AppException : System.ApplicationException
	{
		/// <summary>
		/// 
		/// </summary>
		protected FrameworkDate eventTime;
		
		/// <summary>
		/// 
		/// </summary>
		protected string sourceMethodName;
		
		/// <summary>
		/// 
		/// </summary>
		protected string originalStackTrace;
		
		/// <summary>
		/// 
		/// </summary>
		protected ComponentIdentity componentIdentity;

		/// <summary>
		/// 
		/// </summary>
		protected string sourceHostName;

		public override void GetObjectData(SerializationInfo info, StreamingContext context) 
		{
			base.GetObjectData(info, context);
			info.AddValue("eventTime", this.eventTime, this.eventTime.GetType());
			info.AddValue("sourceMethodName", this.sourceMethodName);
			info.AddValue("originalStackTrace", this.originalStackTrace);
			info.AddValue("componentIdentity", this.componentIdentity, this.componentIdentity.GetType());
			info.AddValue("sourceHostName", this.sourceHostName);
		}

		public AppException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.eventTime = new FrameworkDate();
			this.componentIdentity = new ComponentIdentity();
			this.eventTime = (FrameworkDate) info.GetValue("eventTime", this.eventTime.GetType());
			this.sourceMethodName = info.GetString("sourceMethodName");
			this.originalStackTrace = info.GetString("originalStackTrace");
			this.componentIdentity = (ComponentIdentity) info.GetValue("componentIdentity", this.componentIdentity.GetType());
			this.sourceHostName = info.GetString("sourceHostName");
		}

		/// <summary>
		/// Constructor that takes a message.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="sourceClassName"></param>
		/// <param name="sourceMethodName"></param>
		public AppException(string message, ComponentIdentity componentIdentity, string sourceMethodName) : base(message)
		{
			setAutomaticData();			
			setManualData(componentIdentity, sourceMethodName);
		}

        /// <summary>
		/// Constructor that takes a .NET exception. This constructor is effectively
		/// for converting a .NET exception into an enhanced AppException.
		/// </summary>
		/// <param name="ex">The .NET exception</param>
		/// <param name="sourceClassName"></param>
		/// <param name="sourceMethodName"></param>
		public AppException(System.Exception ex, ComponentIdentity componentIdentity, string sourceMethodName) : base(ex.Message, ex)
		{
			this.originalStackTrace = ex.StackTrace;
			setAutomaticData();			
			setManualData(componentIdentity, sourceMethodName);
		}

		private void setAutomaticData()
		{
			this.eventTime =  new FrameworkDate(System.DateTime.Now);
			try
			{
				this.sourceHostName = System.Net.Dns.GetHostName();
			}
			catch (Exception)
			{
				this.sourceHostName = "UNKNOWN_HOST_ADDRESS";
			}
		}
		
		private void setManualData(ComponentIdentity componentIdentity, string sourceMethodName)
		{
			this.componentIdentity = componentIdentity;
			this.sourceMethodName = sourceMethodName;
		}

        // REPLACED with property EventTime
        //public FrameworkDate getEventTime()
        //{
        //    return this.eventTime;
        //}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>source host name (DNS name) generating the exception</returns>
		public string getSourceHostName()
		{
			return this.sourceHostName;
		}


		public ComponentIdentity getComponentIdentity()
		{
			return this.componentIdentity;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>source method generating the exception</returns>
		public string getSourceMethodname()
		{
			return this.sourceMethodName;
		}
		
		/// <summary>
		/// 
		/// </summary>
		/// <returns>when created from a .NET exception, this is the embedded 
		/// stack trace that was in the .NET exception</returns>
		public string getOriginalStackTrace()
		{
			return this.originalStackTrace;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>localized message</returns>
		public string getMessage()
		{
			return AppResources.getLocalizedString(this.Message);
		}

		public String toString()
		{
            return this.EventTime
				+ "; " + this.GetType().FullName + "-->" + this.getMessage()
				+ "; " + this.componentIdentity.ToString() 
				+ "; " + this.getSourceHostName() 
				+ "; " + this.getSourceMethodname();
		}

		public override String  ToString()
		{
			  return this.toString();
          }


        #region Properties

        public FrameworkDate EventTime
        {
            get
            {
                return this.eventTime;
            }
        }

        # endregion
      }
}
