﻿/*
Copyright (c) 2012, Compass Point, Inc.
All rights reserved.

Redistribution and use when permitted, in source and binary forms, with or without modification, require the following conditions be met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
        in the documentation and/or other materials provided with the distribution.
    Neither the name Compass Point, Inc., nor the names of its contributors, may be used to endorse or promote products 
        derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS 
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;
using System.Reflection;
using System.Runtime.Remoting;

namespace net.compasspoint
{
    public class WcfService : IService
    {
        #region IService Members

        List<ServiceHost> serviceHosts = new List<ServiceHost>();

        public string Startup()
        {
            List<object> services = getConfiguredServices();

            foreach (object service in services)
            {
                ServiceHost serviceHost = new ServiceHost(service);
                serviceHost.Open();
                serviceHosts.Add(serviceHost);

            }

            string endpointStrings = "\n";

            foreach (ServiceHost serviceHost in serviceHosts)
            {
                foreach (ChannelDispatcher channelDispatcher in serviceHost.ChannelDispatchers)
                {
                    foreach (EndpointDispatcher endpointDispatcher in channelDispatcher.Endpoints)
                    {
                        endpointStrings += endpointDispatcher.EndpointAddress.Uri.ToString() + "\n";
                    }
                }
                endpointStrings += "\n";
            }

            return endpointStrings;
        }

        public void Shutdown()
        {
            foreach (ServiceHost serviceHost in serviceHosts)
            {
                serviceHost.Close();
                ((IDisposable)serviceHost).Dispose();
            }

            Logger.shutdown();
        }

        #endregion

        protected List<object> getConfiguredServices()
        {
            List<object> services = new List<object>();

            int i = -1;
            string classTypeName = "";
            string classAssemblyName = "";
            while (true)
            {
                i++;
                try
                {
                    classTypeName = AppResources.getConfigurationString("WcfService.ClassName." + i.ToString());
                    classAssemblyName = AppResources.getConfigurationString("WcfService.AssemblyName." + i.ToString());

                    ObjectHandle obj = Activator.CreateInstance(classAssemblyName, classTypeName);
                    services.Add(obj.Unwrap());

                    Logger.submitToLog(new DiagnosticsLogMessage("SUCCESSFULLY created service [" + classTypeName + "].", new ComponentIdentity(this.GetType().FullName), "getConfiguredServices"));
                }
                catch (ConfigStringNotFoundException)
                {  //stop when no more string.# found.
                    break;
                }
                catch (Exception ex)
                {
                    if (classTypeName.Trim().Equals(""))
                    {
                        Logger.submitToLog(new TraceLogMessage("RESOURCES IS NOT THROWING EXCEPTION WHEN KEY IS NOT IN CONFIG FILE.", new ComponentIdentity(this.GetType().FullName), "getConfiguredServices"));
                        break;
                    }
                    else //if service fails to instantiate, continue onward....
                    {
                        Logger.submitToLog(new ErrorLogMessage("ERROR instantiating [" + classTypeName + "] service. Continuing trying to instantiate other configured services. Exception reports: " + ex.ToString(), new ComponentIdentity(this.GetType().FullName), "getConfiguredServices"));
                    }
                }
            }

            return services;
        }
    }
}