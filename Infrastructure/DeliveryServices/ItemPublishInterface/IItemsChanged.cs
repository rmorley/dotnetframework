﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace com.example.delivery
{
    [ServiceContract()]// [ServiceContract(SessionMode=SessionMode.Required)] requires the transport to support reliable delivery, either TCP, ReliableSessionBinding element, or MSMQ. See http://msdn.microsoft.com/en-us/library/ms733040.aspx
    public interface IItemsChanged
    {
        [OperationContract(IsOneWay = true)] // can't be used with transactions
        //[OperationContract]
        //[TransactionFlow(TransactionFlowOption.Allowed)]
        void allFinishedGoodItems(List<FinishedGoodItem> finishedGoodItems);
    }



    [DataContract]
    public class FinishedGoodItem : Item
    {
        [DataMember]
        public string SalesBrand { get; set; }
 
        [DataMember]
        public string SalesType { get; set; }
 
        [DataMember]
        public string SalesSize { get; set; }

        [DataMember]
        public string SalesStyle { get; set; }

        [DataMember]
        public string SalesSegment { get; set; }

        [DataMember]
        public string SalesPromoGroup { get; set; }
    }


    [DataContract]
    public class Item
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string Description { get; set; }
        
        [DataMember]
        public string UpcCode { get; set; }
        
        [DataMember]
        public string Category { get; set; }
        
        [DataMember]
        public string ForecastControl { get; set; }
        
        [DataMember]
        public string EaToBaseUom { get; set; }
        
        [DataMember]
        public string VolumeUomToBaseUom { get; set; }
        
        [DataMember]
        public string PricingUomToBaseUom { get; set; }
        
        [DataMember]
        public string VolumeUom { get; set; }
        
        [DataMember]
        public string PricingUom { get; set; }
        
        [DataMember]
        public string BaseUom { get; set; }

        [DataMember]
        public DateTime LastUpdateDate { get; set; }

        [DataMember]
        public string StatusCode { get; set; }
    }
}
