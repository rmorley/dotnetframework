﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace com.example.delivery
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class ItemsSubscriberService : IItemsChanged
    {
        #region IItemsChanged Members

        public void allFinishedGoodItems(List<FinishedGoodItem> finishedGoodItems)
        {
            System.Diagnostics.Debug.WriteLine("SERVICE SUBSCRIBER CALLED");
        }

         #endregion
    }
}
