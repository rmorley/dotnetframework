﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace com.example.delivery
{
    public class ItemsLocalSubscriber : IItemsChanged
    {
        #region IItemsChanged Members

        public void allFinishedGoodItems(List<FinishedGoodItem> finishedGoodItems)
        {
            System.Diagnostics.Debug.WriteLine("LOCAL SUBSCRIBER CALLED");
        }

        #endregion
    }
}
