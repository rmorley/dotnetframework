﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using net.compasspoint;
using net.compasspoint.delivery;

//NOTE: MSMQ queues are ordered by priority (see: http://stackoverflow.com/questions/1310414/getting-message-by-priority-from-msmq )
namespace com.example.delivery
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [SubscriberInvocationMode(SubscriberInvocationModeEnum.SequentialOneWay)]
    public class ItemsChangedPublishService : DelivererModel<IItemsChanged>, IItemsChanged
    {
         #region IItemsChanged Members

        //[OperationBehavior(TransactionScopeRequired = true)] if a transaction scope is required
        //NOTE:  this is needed by MSMQ to return message to Q if method fails!! (see: http://msdn.microsoft.com/en-us/library/ms751493.aspx )
        public void allFinishedGoodItems(List<FinishedGoodItem> items)
        {
            deliver(items);
        }

         #endregion
    }
}
