<configuration>
	<system.serviceModel>
		<services>
			<service name ="com.example.delivery.ItemsChangedPublishService">
				<host>
					<baseAddresses>
						<add baseAddress="http://localhost:9060/ItemsChangedPublishService"/>
						<add baseAddress="net.tcp://localhost:9061/ItemsChangedPublishService"/>
					</baseAddresses>
				</host>
				<endpoint
				   address="IItemsChanged"
				   binding="wsHttpBinding"
				   bindingConfiguration="WSHTTP_Reliable"
				   contract="com.example.delivery.IItemsChanged"
				/>
				<endpoint
					address="mex"
					binding="mexHttpBinding"
					contract="IMetadataExchange"/>
				<endpoint
				   address="IItemsChanged"
				   binding="netTcpBinding"
				   bindingConfiguration="TCP_ReliableTransactional"
				   contract="com.example.delivery.IItemsChanged"
				/>
				<!-- for prior endpoint: bindingConfiguration="TCP_Reliable"-->
				<!-- Only needed if MEX over tcp needed. WCF test utility only uses mex over http
				<endpoint
					address="mex"
					binding="mexTcpBinding"
					contract="IMetadataExchange"/>
-->
			</service>
			<service name="net.compasspoint.delivery.SubscribeService">
				<host>
					<baseAddresses>
						<add baseAddress="http://localhost:9080/SubscribeService"/>
						<add baseAddress="net.tcp://localhost:9081/SubscribeService"/>
					</baseAddresses>
				</host>
				<endpoint
					address="ISubscribe"
					binding="wsHttpBinding"
					bindingConfiguration="WSHTTP_Reliable"
					contract="net.compasspoint.delivery.ISubscribe"/>
				<endpoint
					address="mex"
					binding="mexHttpBinding"
					contract="IMetadataExchange"/>
				<endpoint
					address="ISubscribe"
					binding="netTcpBinding"
					bindingConfiguration="TCP_ReliableTransactional"
					contract="net.compasspoint.delivery.ISubscribe"/>
				<!-- Only needed if MEX over tcp needed. WCF test utility only uses mex over http
				<endpoint
					address="mex"
					binding="mexTcpBinding"
					contract="IMetadataExchange"/>
-->
			</service>
	</services>
		<bindings>
			<netTcpBinding>
				<binding name="TCP_Reliable">
					<reliableSession ordered="true" enabled="true" />
					<security mode="Message">
						<message clientCredentialType="Windows" />
					</security>
				</binding>
				<!-- supports setting TransactionProcol property to OLE transactions, WS-Transactions, etc.-->
				<binding name="TCP_ReliableTransactional" transactionFlow="true" >
					<reliableSession ordered="true" enabled="true" />
					<security mode="Message">
						<message clientCredentialType="Windows" />
					</security>
				</binding>
			</netTcpBinding>
			<wsHttpBinding>
				<binding name="WSHTTP_Reliable">
					<reliableSession ordered="true" enabled="true" />
					<security mode="Message">
						<message clientCredentialType="Windows" />
					</security>
				</binding>
				<!-- only supports WS-Transactions-->
				<binding name="WSHTTP_ReliableTransactional" transactionFlow="true" >
					<reliableSession ordered="true" enabled="true" />
					<security mode="Message">
						<message clientCredentialType="Windows" />
					</security>
				</binding>
			</wsHttpBinding>
		</bindings>
		<behaviors>
			<serviceBehaviors>
				<behavior>
					<!-- To avoid disclosing metadata information, 
          set the value below to false and remove the metadata endpoint above before deployment -->
					<!-- having this element enables publishing behavior. Without it, the mex bindings don't do anything. -->
					<serviceMetadata httpGetEnabled="False" />
					<!-- To receive exception details in faults for debugging purposes, 
          set the value below to true.  Set to false before deployment 
          to avoid disclosing exception information -->
					<serviceDebug includeExceptionDetailInFaults="False" />
				</behavior>
			</serviceBehaviors>
		</behaviors>
	</system.serviceModel>
	<appSettings>
    <!-- SERVICE -->
    <!-- false will cause service to run as console application, true requires installing and running as a Windows service -->
    <add key="Service.isService" value="false" />
    <add key="Service.eventsourcestring" value="Items Publish Service" />
    <add key="Service.servicename" value="Items Publish Service" />
    <!-- implements IService -->
    <add key="ServiceAssemblyName" value="WcfService" />
    <add key="ServiceClassNameService" value="net.compasspoint.WcfService" />
    <add key="ServiceAssemblyNameConsole" value="Framework" />
    <add key="ServiceClassNameConsole" value="net.compasspoint.ConsoleBootstrapper" />
    <add key="Service.strStarting" value="Service starting..." />
    <add key="Service.started" value="Service started. {0:G}{0:G}Service assemblies: {1:G}{0:G}Service endpoint uri(s): {2:G}" />
    <add key="Service.error" value="Service start failed. {0:G}{0:G}Service assemblies: {1:G}{0:G}Error message: {2:G}{0:G}Internal error Details: {3:G}." />
    <add key="Service.strStopping" value="Service Stopping..." />
    <add key="Framework.strRunning" value="Service started. {0:G}{0:G}Service assemblies: {1:G}{0:G}Service endpoint uri(s): {2:G}{0:G}Press enter to shutdown the service." />
    <!-- LOGGING -->
    <!-- Configuration to use DefaultLogger - writing straight to file. Only for testing - NOT FOR PRODUCTION USE. -->
    <!--
    <add key="Logger.loggerAssembly" value="Framework" />
    <add key="Logger.loggerclassname" value="net.compasspoint.DefaultLogger" />
     Configuration to use QueuedLogger - write to queue with background thread writing to file on timed basis - FOR PRODUCTION USE
				when sending to the central DiagnosticsLogger is not necessary.
		-->
        
    <add key = "Logger.loggerAssembly" value = "Framework" />
		<add key = "Logger.loggerclassname" value = "net.compasspoint.QueuedLogger" />
		
    <!-- Configuration to use DiagnosticsWebService client - write to queue with background thread writing to file and to the 
				central DiagnosticsWebService on a timed basis - FOR PRODUCTION USE.
		<add key = "Logger.loggerAssembly" value = "DiagnosticWebServiceClient" />
		<add key = "Logger.loggerclassname" value = "net.compasspoint.DiagnosticLogger" />
		-->
    <!-- For setting the log message formatter -->
    <add key="Logger.logmessageformatterclassname" value="net.compasspoint.TestLogMessageFormatter" />
    <!-- For setting the location and filename of the local log file: for DefaultLogger, QueuedLogger, and DiagnosticsWebServiceClient 
				NOTE: when process running as service (Service.isService = true), this will create a directory under c:\windows\system32 and not a subdirectory
				under the directory of the binaries. When running as a console application (Service.isService = false), this will create a 
				directory under the directory storing the executing binaries. -->
    <add key="Defaultlogger.logpath" value="log\server.log" />
    <!-- Settings for QueuedLogger and DiagnosticsWebServiceClient -->
    <add key="Queuedlogger.threadshutdownmillis" value="10000" />
    <add key="Queuedlogger.repeatintervalmillis" value="300000" />

    <add key = "WcfService.AssemblyName.0" value = "ItemPublishServiceLibrary" />
    <add key = "WcfService.ClassName.0" value = "com.example.delivery.ItemsChangedPublishService" />

    <add key = "WcfService.AssemblyName.1" value = "SubscribersServiceLibrary" />
		<add key = "WcfService.ClassName.1" value = "net.compasspoint.delivery.SubscribeService" />


	</appSettings>
  <connectionStrings>
	  <add name="SubscribersModelContainer" connectionString="metadata=res://*/SubscribersModel.csdl|res://*/SubscribersModel.ssdl|res://*/SubscribersModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;Data Source=localhost\sqlexpress;Initial Catalog=Subscribers;Integrated Security=True;MultipleActiveResultSets=True&quot;" providerName="System.Data.EntityClient" />
  </connectionStrings>
  <system.web>
    <compilation debug="true" />
  </system.web>
</configuration>